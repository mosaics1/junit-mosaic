## Tools for asserting that objects get released

junit-mosaic offers two tools for asserting that objects get reclaimed by the JVMs garbage collector.

* JMAssertions.spinUntilReleased(reference);
* GCBarrier

Use JMAssertions for tracking a single reference and GCBarrier for tracking multiple objects.

### JMAssertions.spinUntilReleased

```
   DTO                dto = new DTO("foo");
   WeakReference<DTO> ref = new WeakReference<>( dto );
    
   dto = null; // remove the only strong reference to DTO so that it becomes GC'able
    
   JMAssertions.spinUntilReleased( ref );  // blocks here until the dto is GC'd
                                           // it will time out after n seconds
                                           // where n defaults to 6s, and can be changed
                                           // by setting an environment variable (mosaics.junit.TimeoutSeconds).
    
   assertNull( ref.get() );   // will never fail - to verify, delete the spin above
```

### GCBarrier

```
   GCBarrier GCBarrier = new GCBarrier();

   GCBarrier.push( new Object() );
   GCBarrier.push( new Object() );

   GCBarrier.spinUntilAllObjectsHaveBeenReleased();
```


