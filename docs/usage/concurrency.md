## Tools for testing concurrent code

When another thread is carrying out a calculation and we need the current thread to wait for
some situation to be reached.  The challenge is that we do not know how long to wait. Wait too
long and the tests will run slow.  Fail to wait long enough  and the test will fail.  And because
the threads may take different lengths of time on each run, the length of time that is too
long or too short changes which creates a situation where test flicker between success and failure
every time the are run.  What we want is for the test to be reliable and to complete as quickly
as possible.  This situation is only made worse when considering we often run tests
in different environments, and each environment can have very difference performance characteristics.

The following tools solve the 'flickering' problems with blocking waits that were discussed above
by offering the ability to keep checking until the job is done or a limit has been reached.  Similar
to children on a car ride repeatedly asking 'are we there yet?'.    

    JMAssertions.spinUntilTrue( x -> someCondtionCheck() )
    JMAssertions.spinUntilTrue( x -> someCondtionCheck() )
    JMAssertions.spintUntilFalse( x -> someCondtionCheck() )
    JMAssertions.spinUntilStable( () -> fetchAValue() )
    JMAssertions.awaitLatch(java.util.concurrent.CountDownLatch)

The default max wait for each of these methods is 6 seconds, each method has the option of having
an override duration specified and te global default can be setting the system parameter
mosaics.junit.TimeoutSeconds.

    -Dmosaics.junit.TimeoutSeconds=6

Another problem that can occur is in environments where a JVM effectively 'stalls' for a period of
time at random intervals also resulting in flickering builds.  Think of a CI server that is getting
time sliced out in a virtualised environment running in the cloud.  For this reason all spin
and wait methods exclude any time where the JVM was effectively stalled from the timeout period.
For example, if a test method was to spin for 8 seconds *and* the JVM stalled for 3 of those seconds
then it would still pass subtracting the stalled time from the total and we get 5 seconds (8-3 < 6). 

The polling tools have two optional parameters, the max length of time
to poll before failing and how long to wait between checks.  The shorter the interval, the
more responsive the test will be to when the condition is reached and the max wait can be safely
quite large as it will only slow the tests when the tests are failing.  Which is often a fair
trade off.



For more details see [JMAssertions.java](src/main/java/mosaics/junit/JMAssertions.java)
