## Tools for providing random data to a test

Fields and parameters on the test class that are marked with @Random will be provided with a random
value.

    @ExtendWith(JMRandomExtension.class)
    public class JMRandomExtensionTest {
        private @Random int field1;
        
        @Test
        public void testRandomString( @Random String rndName ) {
            assertNotNull( rndName );
        }
        
        @Test
        public void testRandomField() {
            assertIsNotZero( field1 );
        }
        
        @Test
        public void testRandomStringDto( @Random DTO rndBean ) {
            assertNotNull( rndBean );
        }
    }

### Random Seeds

When random tests start to fail, it is useful to make them reproducible.  To do that, specify the
starting random seed for the test.

    @RandomSeed(1234L)
    @ExtendWith(JMRandomExtension.class)
    public class JMRandomExtensionTest {
        @Test
        public void testRandomString( @Random String rndName ) {
            assertNotNull( rndName );
            fail();
        }
    }

When a test fails with an exception, it will report the random seed tht was used 
for that test.  Which means, if code being tested calls random in the same order
then by setting the seed of the next test run then you will be able to reproduce
the previous fail.  Thus solving a wonderful problem with random tests, reproducing
failures. 


### Supported types

* primitives and their object equivalents (boolean/Boolean, byte/Byte etc)
* enums and strings
* list, set, map, optional, arrays
* static factory methods with the signature Object rnd(Function<Class,Object>)
* static methods whose arguments are supported by @Random (when there
  are multiple options, the static method with the most arguments is selected)  
* classes with a public constructor that takes arguments that are supported by @Random
  (when there are multiple constructors, the constructor that takes the most arguments
  will be selected)

            
### Custom types

Sometimes the default behaviour for randomly generating an object is not enough
and we need to take over with some custom code.  Perhaps to put constraints in 
or to make the data look more realistic.  To do this, add a static factory
method to the class called 'rnd' like this:

    @Value
    public class MyBean {
        public static MyBean rnd( Function<Class,Object> factory ) {
            return new MyBean( (String) factory.apply(String.class) );
        }
    
        private final String name;
    }

The static factory method takes a factory method as an argument.  This argument
can be used to request objects to be randomly created.  See the example above.

To make the factory method a little nicer to use, we recommend adding a copy
of the following RandomFactory class to your code base.

    public class RandomFactory {
        private Function<Class,Object> f;
    
        public RandomFactory(Function<Class,Object> f) {
            this.f = f;
        }
    
        public<T> T rnd( Class<T> type ) {
            return (T) f.apply( type );
        }
    }
 
By adding the RandomFactory class to your code base, one can rewrite MyBean
to look like this:

    @Value
    public class MyBean {
        public static MyBean rnd( RandomFactory factory ) {
            return new MyBean( factory.rnd(String.class) );
        }
    
        private final String name;
    }

### Overriding object creation

The final option for custom generation is at the site where the @Random annotation
is used.  @Random takes a 'generator' argument.  The 'generator' is a class that
implements java.util.Supplier.  For example:

    public class GeneratorA implements Supplier<String> {
        public String get() {
            return "A";
        }
    }
    
The constructor of the Generator may take Random, RandomBeanGenerator, the field or 
parameter name (String) and the target type to be generated (Class).  For example
consider one of the provided generators at Generators.java:

    @AllArgsConstructor
    public static class PhoneGenerator implements Supplier<String> {
        private final Random random;
    
        @Override
        public String get() {
            return random.nextPhone();
        }
    }

### Strings

String generation, by default uses a lorem ipsum generator courtest of
https://github.com/mdeanda/lorem.  And to ensure that non latin characters are
supported, every now and then Japense text will be generated using a corpus 
of Kanji generated from https://generator.lorem-ipsum.info/_japanese.


