## Tools for ensuring that threads get cleaned up

JMThreadExtension detects threads that get left running by a unit test.  It thus enforces that
unit tests clean up after themselves.  See JMThreadExtensionTest for more examples.

    @Isolated   // include @Isolated to prevent threads from other tests from being picked up 
    @ExtendWith( JMThreadExtension.class )
    @JMThreadExtension.IgnoreThread("Attach Listener")
    @JMThreadExtension.IgnoreThread("org.junit.*")
    public class TestClass {
        
    }

To tell the test that some new threads are okay, such as daemon threads that get started
once and never get shutdown then @IgnoreThread can be used.  It accepts a regular
expression that is matched against both thread names and thread class names.

    @Isolated 
    @ExtendWith( JMThreadExtension.class )
    @JMThreadExtension.IgnoreThread("Attach Listener")
    @JMThreadExtension.IgnoreThread("org.junit.*")
    public class TestClass {
        
    }

