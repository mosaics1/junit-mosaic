## Tools for creating and cleaning up files used during testing

For unit tests that access files in a directory, the following junit extension can be used to
prepopulate the directories/files for use by the test.  The files will be placed before the test
starts, and will be cleaned up after the test.  The files will be created in their own per test
area, thus ensuring that even if all of the tests are run concurrently then there will be no file clashes
between the tests.  The only files that will be shared between tests are directories/files declared
using static fields.

See the following examples:

```
    @ExtendWith(JMFileExtension.class)
    public class Test {
        // static fields are populated @BeforeAll tests are run
        // and removed as part of @AfterAll.
        private static File anEmptyDirectorySharedByAllTests;
    
        // a field of type File with no further annotations will be populated with a reference
        // to a new created and empty directory that will also be deleted after the tests have been
        // run.
        private File anEmptyDirectory;
    
        // Use of the @FileContents annotation changes File from being a directory to a file populated
        // with the specified lines of text.  In this case 'abc' and 'def'.
        @FileContents(lines={"abc","def"})
        private File fileWithContents;
    
        // @FileContents supports multiline strings and if the first non-whitespace character of
        // a line is '|' then all whitespace that line up to and including the '|' will be stripped out.
        // For example, the contents of the following file will be the same as {"abc def", "123 456"}.
        @FileContents(lines={"""
            |abc def
            |123 456
            """
        })
        private File fileWithContents;
    
        // A files last modified timestamp can also be specified explicitely.  If it is not specified 
        // then the current system time will be used.
        @FileContents(lines={"abc","def"}, lastModified="2020-05-10T10:44:11Z")
        private File fileWithContents;
    
        // Use of the @FileContents annotation can also load a resource from the classpath.  Relative
        // resource paths will be relative to the class that the field is declared upon.
        @FileContents(resource="fileA.dat")
        private File fileCopiedFromClassPath;
    
        // When we want to prepopulate a directory with multiple files, @DirectoryContents allows
        // the contents of multiple files to be specified in filename/contents pairs.  The field will
        // be populated with the root directory where all of the files will be placed at.
        @DirectoryContents({
                @FileContents(name="a.txt", lines={"abc"}),
                @FileContents(name="foo/bar.txt", lines={"123"}),
                @FileContents(name="/a/b/c.txt", lines={"123456"})
        })
        private File directoryWithContents;
    
        // Files may also be specified on the test method as a method parameter. 
        public void testMethod( File tmpDir ) {
            // tmpDir will be created on disk just before this method is called,
            // and it and all of its contents will be deleted after the method has finished
        }
    }
```
