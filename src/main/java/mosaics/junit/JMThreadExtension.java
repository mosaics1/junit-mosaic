package mosaics.junit;

import mosaics.junit.jvm.ThreadWatcher;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Supplier;


/**
 * JMThreadExtension detects threads that get left running by a unit test.  It thus enforces that
 * unit tests clean up after themselves.
 *
 * <code>
 * @ExtendWith( JMThreadExtension.class )
 * @JMThreadExtension.IgnoreThread("Attach Listener")
 * @JMThreadExtension.IgnoreThread("org.junit.*")
 * public class TestClass {
 *
 * }
 * </code>
 */
public class JMThreadExtension
    implements BeforeAllCallback, AfterAllCallback, BeforeEachCallback, AfterEachCallback {

    @Repeatable(IgnoreThreads.class)
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.TYPE, ElementType.METHOD})
    public static @interface IgnoreThread {
        /**
         * Specify a thread to be ignored by its thread name or threads fully qualified class name.
         * Uses regular expression syntax.
         */
        public String value() default "";
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.TYPE, ElementType.METHOD})
    public static @interface IgnoreThreads {
        public IgnoreThread[] value();
    }



    private static enum TestScope {BEFORE_ALL, BEFORE_EACH}

    private final ExtensionContext.Namespace storeNamespace = ExtensionContext.Namespace.create(this.getClass().getName());

    @Override
    public void beforeAll( ExtensionContext context ) throws InterruptedException {
        startWatchingThreads( TestScope.BEFORE_ALL, context );
    }

    @Override
    public void afterAll( ExtensionContext context ) {
        stopWatchingThreads( TestScope.BEFORE_ALL, context );
    }

    @Override
    public void beforeEach( ExtensionContext context ) throws InterruptedException {
        startWatchingThreads( TestScope.BEFORE_EACH, context );
    }

    @Override
    public void afterEach( ExtensionContext context ) {
        stopWatchingThreads( TestScope.BEFORE_EACH, context );
    }


    private void startWatchingThreads( TestScope scope, ExtensionContext context ) throws InterruptedException {
        ThreadWatcher threadWatcher = getThreadWatcher( scope, context );

        threadWatcher.startThreadCleanupCriticalRegion();
    }

    private void stopWatchingThreads( TestScope scope, ExtensionContext context ) {
        ThreadWatcher threadWatcher = getThreadWatcher( scope, context );

        threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();
    }

    private ThreadWatcher getThreadWatcher( TestScope scope, ExtensionContext context ) {
        ExtensionContext.Store store = context.getStore( storeNamespace );

        return store.getOrComputeIfAbsent( "ThreadWatcher "+scope, key -> createNewThreadWatcher(context), ThreadWatcher.class );
    }

    private ThreadWatcher createNewThreadWatcher( ExtensionContext context ) {
        ThreadWatcher threadWatcher = new ThreadWatcher();

        registerClassIgnoreThreadAnnotations( context, threadWatcher );
        registerMethodIgnoreThreadAnnotations( context, threadWatcher );

        threadWatcher.ignore( "org.junit.*" );
        threadWatcher.ignore( "Attach Listener" );

        return threadWatcher;
    }

    private void registerMethodIgnoreThreadAnnotations( ExtensionContext context, ThreadWatcher threadWatcher ) {
        context.getTestMethod().ifPresent(
            m -> registerIgnoreThreadAnnotations( threadWatcher, () -> m.getAnnotationsByType( IgnoreThread.class) )
        );
    }

    private void registerClassIgnoreThreadAnnotations( ExtensionContext context, ThreadWatcher threadWatcher ) {
        registerIgnoreThreadAnnotations(
            threadWatcher,
            () -> context.getRequiredTestClass().getAnnotationsByType(IgnoreThread.class)
        );
    }

    private void registerIgnoreThreadAnnotations( ThreadWatcher threadWatcher, Supplier<IgnoreThread[]> annotationFetcher ) {
        registerIgnoreThreadAnnotations( threadWatcher, annotationFetcher.get() );
    }

    private void registerIgnoreThreadAnnotations( ThreadWatcher threadWatcher, IgnoreThread[] annotations ) {
        for ( IgnoreThread annotation : annotations ) {
            registerIgnoreThreadAnnotationWith( threadWatcher, annotation );
        }
    }

    private void registerIgnoreThreadAnnotationWith( ThreadWatcher threadWatcher, IgnoreThread ignoreThreadAnnotation ) {
        threadWatcher.ignore( ignoreThreadAnnotation.value() );
    }
}
