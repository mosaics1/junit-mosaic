package mosaics.junit;

import mosaics.junit.beans.DefaultGenerator;
import mosaics.junit.beans.RandomBeanGenerator;
import mosaics.junit.di.ObjectFactory;
import mosaics.junit.lang.reflection.JavaParameter;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;


/**
 * Supports generating random values for test class fields and test method parameters.
 *
 * <code>
 * @Test
 * public void f( @Random String v ) {
 *
 * }
 * </code>
 *
 * For tests that start to randomly fail, the failing test provides the random seed used
 * to start the random number generator backing the tests.  The test can then be reproduced
 * by specifying the initial random seed.
 *
 * <code>
 * @RandomSeed(123L)
 * public class Tests {
 *
 * }
 * </code>
 */
public class JMRandomExtension extends JMExtension {
    private static final String RANDOM_SEED_KEY = "RANDOM_SEED";

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.PARAMETER})
    public static @interface Random {
        public Class generator() default DefaultGenerator.class;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.TYPE})
    public static @interface RandomSeed {
        public long value();
    }

    public static class RandomSeedDecoratedException extends RuntimeException {
        public RandomSeedDecoratedException( Throwable chainedException, long seed ) {
            super( "Random seed at the start of this test was '"+seed+"'", chainedException );
        }
    }

    public JMRandomExtension(  ) {
        setObjectFactoryFactory( this::fetchOrCreateObjectFactory );
    }

    private static class RandomObjectFactory implements ObjectFactory {
        private final RandomBeanGenerator generator;

        private RandomObjectFactory( long seed ) {
            this( new RandomBeanGenerator(seed) );
        }

        private RandomObjectFactory( RandomBeanGenerator generator ) {
            this.generator = generator;
        }

        public boolean supports( JavaParameter param ) {
            return param.hasAnnotation(Random.class) && generator.supports( param.type() );
        }

        public Object create( JavaParameter param ) {
            return generator.rnd( param );
        }

        public Optional<Object> get( JavaParameter param ) {
            return Optional.empty();
        }

        public void releaseAll() {}
    }

    @Override
    public void handleTestExecutionException( ExtensionContext context, Throwable throwable ) throws Throwable {
        long      seed             = getOrInitSeed( context );
        Throwable wrappedThrowable = new RandomSeedDecoratedException(throwable, seed);

        super.handleTestExecutionException( context, wrappedThrowable );
    }

    private long selectNextRandomSeed( ExtensionContext context ) {
        return getRandomSeedAnnotation(context)
            .map( RandomSeed::value )
            .orElseGet( System::nanoTime );
    }

    private Optional<RandomSeed> getRandomSeedAnnotation( ExtensionContext context ) {
        return context.getTestClass()
            .flatMap(c -> Optional.ofNullable(c.getAnnotation(RandomSeed.class)) );
    }

    private ObjectFactory fetchOrCreateObjectFactory( ExtensionContext context ) {
        long seed = getOrInitSeed( context );

        return new RandomObjectFactory(seed);
    }

    private long getOrInitSeed( ExtensionContext context ) {
        return (long) getContextStore( context )
            .getOrComputeIfAbsent( RANDOM_SEED_KEY, k -> selectNextRandomSeed(context) );
    }
}
