package mosaics.junit;

import mosaics.junit.di.ObjectFactory;
import mosaics.junit.lang.ReflectionUtils;
import mosaics.junit.lang.reflection.JavaParameter;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static mosaics.junit.lang.ReflectionUtils.isNestedJUnitTestClass;


/**
 * JMExtension is a base class for creating JUnit5 extensions that pre-populates fields on the test
 * classes and parameters on test methods.  In order to know what to inject into the
 * fields/parameters, JMExtension uses an ObjectFactory.  To use JMExtension, extend JMExtension
 * and pass an ObjectFactory to this class via its constructor.  Then use your new extension as
 * a JUnit5 extension using the @ExtendWith annotation placed onto the target test class.<p/>
 *
 * The ObjectFactory takes details about the field or parameter and returns an object that
 * will fit into that field or parameter.  Objects that implement java.lang.AutoCloseable will
 * have their close() methods called when the test ends;  thus giving them the chance to clean
 * up any resources such as files or sockets that they may have allocated during the test.<p/>
 *
 * Class fields (declared static) will be set once before any test has been run and will be released
 * once all of the test methods have been released.  Object instance fields are reset between every
 * test method, and the test method parameters are already scoped to a test method.<p/>
 *
 * To use:
 *   1) extend JMExtension to provide custom instances of ObjectFactory
 *   2) for auto detection by junit, declare the extension within the module-info.java else use it
 *      directly
 */
public abstract class JMExtension implements ParameterResolver,
    BeforeAllCallback, AfterAllCallback,
    BeforeEachCallback, AfterEachCallback, //AfterTestExecutionCallback,
    TestExecutionExceptionHandler
{
    /*
       NB notes on JUnit extension life cycle,
       + Each class could be in different threads, methods within a class are always in the same thread
           - The same instance of JMExtension is used for all methods within a test class.  Nested test
       classes also get their own instance of JMExtension
           - Different ExtensionContext's are used for each test method (and beforeAll/afterAll pairs)
       + When each method can be concurrent
           - All bets are off.  Each method can have its own instance of JMExtension and all state
             has to be past through the test execution context.
           - Before/After all events will be called ahead/after all method invocations and that works
             across nested classes.  That is the parent test class will not have afterAll invoked before
             the nested class has had its afterAll invoked too.

       JUnit Flags for Concurrent classes, serial methods:
           -Djunit.jupiter.execution.parallel.enabled=true
           -Djunit.jupiter.execution.parallel.mode.default=same_thread
           -Djunit.jupiter.execution.parallel.mode.classes.default=concurrent

       JUnit Flags for Concurrent classes, concurrent methods:
           -Djunit.jupiter.execution.parallel.enabled=true
           -Djunit.jupiter.execution.parallel.mode.default=concurrent
           -Djunit.jupiter.execution.parallel.mode.classes.default=concurrent
     */


    /**
     * The default objectFactory returns 'no match'.  Factories that actually do work wrap the existing
     * factory forming a chain as part of the 'plugin' mechanism.
     */
    private       Function<ExtensionContext,ObjectFactory> objectFactoryFactory;
    private final ExtensionContext.Namespace storeNamespace = ExtensionContext.Namespace.create(this.getClass().getName());


    public JMExtension() {}

    public JMExtension( Supplier<ObjectFactory> objectFactoryFactory ) {
        setObjectFactoryFactory( context -> objectFactoryFactory.get() );
    }

    protected void setObjectFactoryFactory( Function<ExtensionContext,ObjectFactory> objectFactoryFactory ) {
        this.objectFactoryFactory = objectFactoryFactory;
    }

    public void beforeAll( ExtensionContext context ) {
        ObjectFactory objectFactory = getStaticObjectFactory( context );

        supportedFields(objectFactory, context.getRequiredTestClass()).forEach( f -> injectIntoField(objectFactory, null, f) );
    }

    public void afterAll( ExtensionContext context ) {
//        clearStaticFields(context.getRequiredTestClass());
        ObjectFactory objectFactory = getStaticObjectFactory( context );

        // NB nested classes cannot have their own static methods
        clearStaticFieldsFor( objectFactory, context.getRequiredTestClass() );

        objectFactory.releaseAll();
    }


    // called before the setup methods; which means that we can inject into the teardown methods too
    @SuppressWarnings("ConstantConditions")
    public void beforeEach( ExtensionContext context ) {
        ObjectFactory objectFactory = getObjectFactory( context );
        Object        testInstance  = context.getRequiredTestInstance();


        // JUnit5 added support for @Nested classes.  They are tests that are written on an anonymous
        // inner classes that have a field called this$0 which point back to the test.   Fields may need
        // to be injected into any of them.

        allTestInstances(testInstance).forEach( o -> injectIntoFieldsFor(objectFactory,o) );
    }

    // called after the tear down methods; which means that we can inject into the teardown methods too
    public void afterEach( ExtensionContext context ) {
        ObjectFactory objectFactory = getObjectFactory( context );
        Object testInstance = context.getRequiredTestInstance();

        allTestInstances(testInstance).forEach( o -> clearAllFieldsFor(objectFactory, o) );

        getObjectFactory(context).releaseAll();
    }



    @SuppressWarnings("unchecked")
    protected static <T extends Annotation> Optional<T> extractAnnotationFrom( Class<T> type, List<Annotation> annotations ) {
        return (Optional<T>) annotations.stream().filter( a -> type.isAssignableFrom(a.getClass()) ).findFirst();
    }


    protected ExtensionContext.Store getContextStore( ExtensionContext context ) {
        return context.getStore( storeNamespace );
    }

    private ObjectFactory getStaticObjectFactory( ExtensionContext context ) {
        ExtensionContext.Store store = getContextStore( context );

        return store.getOrComputeIfAbsent( "StaticObjectFactory", key -> createNewStaticObjectFactory(context), ObjectFactory.class );
    }

    private ObjectFactory getObjectFactory( ExtensionContext context ) {
        ExtensionContext.Store store = getContextStore( context );

        return store.getOrComputeIfAbsent( "ObjectFactory", key -> createNewObjectFactory(context), ObjectFactory.class );
    }

    private ObjectFactory createNewStaticObjectFactory( ExtensionContext context ) {
        ObjectFactory              newFactory    = objectFactoryFactory.apply(context);
        Optional<ExtensionContext> parentContext = context.getParent();

        if ( parentContext.isPresent() ) {
            ObjectFactory parentObjectFactory = getStaticObjectFactory( parentContext.get() );

            newFactory = linkObjectFactories(  newFactory, parentObjectFactory );
        }

        return newFactory;
    }

    private ObjectFactory createNewObjectFactory( ExtensionContext context ) {
        ObjectFactory              newFactory    = objectFactoryFactory.apply(context);
        Optional<ExtensionContext> parentContext = context.getParent();

        if ( parentContext.isPresent() ) {
            ObjectFactory parentObjectFactory = getObjectFactory( parentContext.get() );

            newFactory = linkObjectFactories(  newFactory, parentObjectFactory );
        }

        return newFactory;
    }

    private ObjectFactory linkObjectFactories( ObjectFactory a, ObjectFactory b ) {
        return a.or( b );
    }



    public boolean supportsParameter( ParameterContext parameterContext, ExtensionContext context ) throws ParameterResolutionException {
        JavaParameter key           = toObjectKey( parameterContext );
        ObjectFactory objectFactory = getObjectFactory( context );

        return objectFactory.supports( key );
    }

    public Object resolveParameter( ParameterContext parameterContext, ExtensionContext context ) throws ParameterResolutionException {
        JavaParameter paramDetails  = toObjectKey(parameterContext);
        ObjectFactory objectFactory = getObjectFactory( context );

        return objectFactory.create( paramDetails );
    }

    private JavaParameter toObjectKey( ParameterContext ctx ) {
        Parameter parameter = ctx.getParameter();

        return JavaParameter.of(parameter, Map.of());
    }

    public void handleTestExecutionException( ExtensionContext context, Throwable throwable ) throws Throwable {
//        CachedObjectFetcher fetcher = store.get(context.getUniqueId());
//
//        fetcher.notifyAllReferencesOfException( throwable );
//
        throw throwable;
    }


    private Stream<Field> supportedFields(ObjectFactory objectFactory, Class testClass) {
        return supportedFields(objectFactory, testClass, null);
    }

    private Stream<Field> supportedFields(ObjectFactory objectFactory, Class testClass, Object testInstance) {
        Predicate<Field> fieldScopePredicate = testInstance == null ? ReflectionUtils::isStatic : ReflectionUtils::isInstanceField;

        return ReflectionUtils.allFieldsFor(testClass)
            .filter( fieldScopePredicate )
            .filter( ReflectionUtils::isNotSyntheticField )
            .filter( f -> ReflectionUtils.isUnset(testInstance, f) )
            .filter( objectFactory::supports );
    }

    private Stream<Object> allTestInstances( Object rootTestInstance ) {
        return Stream.of( rootTestInstance ).flatMap( tc -> {
            if ( isNestedJUnitTestClass(tc) ) {
                return Stream.concat(
                    Stream.of(tc),
                    allTestInstances(ReflectionUtils.getContainingObjectForAnonymousInnerClass(tc))
                );
            } else {
                return Stream.of( tc );
            }
        } );
    }

    private void injectIntoField( ObjectFactory objectFactory, Object testInstance, Field destField ) {
        JavaParameter key       = JavaParameter.of(destField);
        Object        newObject = objectFactory.create(key);

        ReflectionUtils.setField( testInstance, destField, newObject );
    }

    private void clearStaticFieldsFor( ObjectFactory objectFactory, Class targetClass ) {
        supportedFields(objectFactory, targetClass, null).forEach( f -> ReflectionUtils.clearField(f, null) );
    }

    private void injectIntoFieldsFor( ObjectFactory objectFactory, Object targetObject ) {
        supportedFields(objectFactory, targetObject.getClass(), targetObject).forEach( f -> injectIntoField(objectFactory, targetObject, f) );
    }

    private void clearAllFieldsFor( ObjectFactory objectFactory, Object targetObject ) {
        supportedFields(objectFactory, targetObject.getClass(), targetObject).forEach( f -> ReflectionUtils.clearField(f,targetObject) );
    }
}
