package mosaics.junit;

import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;


/**
 * Run JUnit tests directly.  Specify a class and run all of the methods annoted with @Test.<p/>
 *
 * This is useful when there is a need to control the running environment of JUnit;  for example,
 * isolating the calls of @BeforeAll so that they do not interfere with other tests.
 *
 * <code>
 * public class ExampleTest {
 *     @Test
 *     public void f() {
 *         JUnitHarness.runUnitTests( F.class );
 *     }
 *
 *     // NB note the lack of @Nested, this avoids some JUnit harnesses from running the test methods
 *     // on this class (eg intellij);  however other harnesses such as gradle still run it.. so
 *     // include a tag and be sure to configure gradle to exclude all tests with that tag.
 *     @Tag("internal")
 *     public static class F {
 *         @Test
 *         public void f() {
 *           // the actual test
 *         }
 *     }
 * }
 * </code>
 */
public class JUnitHarness {
    public static final record FailureSummary( String source, Throwable ex ) {
        public FailureSummary( TestExecutionSummary.Failure failure ) {
            this( failure.getTestIdentifier().getDisplayName(), failure.getException() );
        }
    }

    public static void runUnitTests( Class testClass ) throws MultipleFailuresError {
        SummaryGeneratingListener listener = new SummaryGeneratingListener();

        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
            .selectors(selectClass(testClass))
            .build();

        Launcher launcher = LauncherFactory.create();
        launcher.discover(request);
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);

        List<FailureSummary> failures = listener.getSummary().getFailures().stream()
            .map( FailureSummary::new )
            .collect( Collectors.toList() );

        if ( failures.size() > 0 ) {
            throw new MultipleFailuresError( failures );
        }
    }

    public static class MultipleFailuresError extends AssertionError {
        private List<FailureSummary> failures;

        public MultipleFailuresError( List<FailureSummary> failures ) {
            this.failures = failures;
        }

        public String getMessage() {
            StringBuilder buf = new StringBuilder();

            buf.append("The following");

            if ( failures.size() == 1 ) {
                buf.append( " test" );
            } else {
                buf.append( " " );
                buf.append( failures.size() );
                buf.append( " tests" );
            }

            buf.append(" failed: ");
            buf.append(failures.stream().map(FailureSummary::source).collect(Collectors.joining(", ")));

            return buf.toString();
        }
    }
}
