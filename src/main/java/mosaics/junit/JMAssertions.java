package mosaics.junit;

import mosaics.junit.jvm.PauseDetecter;
import mosaics.junit.lang.RunnableWithThrows;
import org.junit.jupiter.api.Assertions;
import org.opentest4j.AssertionFailedError;

import java.lang.ref.Reference;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Set of useful unit test assertions.
 */
public class JMAssertions extends Assertions {
    public static Duration DEFAULT_TIMEOUT = Duration.ofSeconds( Integer.parseInt(System.getProperty("mosaics.junit.TimeoutSeconds", "6")) );

    public void assertNotZero( int v ) {
        assertTrue( v > 0, v + " should have been > 0" );
    }

    public void assertIsNotZero( int v ) {
        assertTrue( v != 0, v + " should have been == 0" );
    }

    public void assertStreamsEqual( Stream a, Stream b ) {
        assertEquals( a.collect(Collectors.toList()), b.collect(Collectors.toList()) );
    }

    /**
     * Why wait for a memory leak to take a server down.  Verify that an object added to a new collection
     * becomes available for GC when popped from the new data structure.  If the contents of the specified
     * reference does not get released within the default time period then the assertion will fail.
     *
     * <code>
     *    DTO                dto = new DTO("foo");
     *    WeakReference&lt;DTO&gt; ref = new WeakReference&lt;&gt;( dto );
     *
     *    dto = null;                             // remove the only strong reference to DTO so that it become GC'able
     *
     *    JMAssertions.spinUntilReleased( ref );  // spins here until the dto is GC'd
     *
     *    assertNull( ref.get() );                // will never fail - to verify, delete the spin above
     * </code>
     */
    public static void spinUntilReleased( Reference weakReference ) {
        spinUntilTrue( () -> {System.gc(); return weakReference.get() == null;} );
    }

    /**
     * Why wait for a memory leak to take a server down.  Verify that an object added to a new collection
     * becomes available for GC when popped from the new data structure.  If the contents of the specified
     * reference does not get released within the supplied time period then the assertion will fail.
     *
     * <code>
     *    DTO                dto = new DTO("foo");
     *    WeakReference&lt;DTO&gt; ref = new WeakReference&lt;&gt;( dto );
     *
     *    dto = null;                             // remove the only strong reference to DTO so that it become GC'able
     *
     *    JMAssertions.spinUntilReleased( ref );  // spins here until the dto is GC'd
     *
     *    assertNull( ref.get() );                // will never fail - to verify, delete the spin above
     * </code>
     *
     * NB when working with SoftReferences, they will not get GCed until there is memory pressure on
     * the JVM.
     *
     * @see #createMemoryPressure
     */
    public static void spinUntilReleased( Reference ref, Duration timeout ) {
        spinUntilTrue(
            () -> {System.gc(); return ref.get() == null;},
            timeout,
            () -> "Object not released within " + timeout.toMillis()/1000.0 + "s"
        );
    }

    /**
     * To encourage the JVM to release soft references, generate an OutOfMemoryError.
     *
     * Recommend marking tests that use createMemoryPressure with @Isolated (JUnit 5.7).
     */
    public static void createMemoryPressure() {
        // TODO assert that the calling unit test is declared with @Isolated

        // NB an alternative is to specify -XX:SoftRefLRUPolicyMSPerMB=2500;  it will cause
        // soft references to get cleared out after 2.5 seconds

        // credit goes to https://stackoverflow.com/questions/3785713/how-to-make-the-java-system-release-soft-references
        try {
            final List<long[]> memhog = new LinkedList<>();
            while(true) {
                memhog.add(new long[102400]);
            }
        } catch( OutOfMemoryError ex ) {
            /* At this point all SoftReferences have been released - GUARANTEED. */
        }
    }

    /**
     * Repeatedly calls the function f until it returns true.  If the method fails to return true
     * within the default time period then the assertion will fail.
     */
    public static void spinUntilTrue( Supplier<Boolean> f ) {
        spinUntilTrue( f, DEFAULT_TIMEOUT );
    }

    /**
     * Repeatedly calls the function f until it returns true.  If the method fails to return true
     * within the supplied time period then the assertion will fail.
     *
     * @param timeout the max length of time to wait for the predicate to become true, excluding periods
     *                of time where the JVM is estimated to have stalled
     */
    public static void spinUntilTrue( Supplier<Boolean> f, Duration timeout ) {
        spinUntilTrue( f, timeout, () -> "Function did not return true within " + timeout.toMillis() + " millis" );
    }

    /**
     * Repeatedly calls the function f until it returns true.  If the method fails to return true
     * within the supplied time period then the assertion will fail.
     *
     * @param errorMessage supply a custom message for when this method fails
     */
    public static void spinUntilTrue( Supplier<Boolean> f, Supplier<String> errorMessage ) {
        spinUntilTrue( f, DEFAULT_TIMEOUT, errorMessage );
    }

    /**
     * Repeatedly calls the function f until it returns true.  If the method fails to return true
     * within the supplied time period then the assertion will fail.
     *
     * @param timeout the max length of time to wait for the predicate to become true, excluding periods
     *                of time where the JVM is estimated to have stalled
     * @param errorMessage supply a custom message for when this method fails
     */
    public static void spinUntilTrue( Supplier<Boolean> f, Duration timeout, Supplier<String> errorMessage ) {
        long nowMillis = System.currentTimeMillis();
        long maxMillis = nowMillis + timeout.toMillis();

        long initiallStallMillis = PauseDetecter.getTotalStallTimeMillis(); // adjust for possible jvm stalls

        int exceptionCount = 0;
        Throwable firstException = null;
        while ( System.currentTimeMillis() - (PauseDetecter.getTotalStallTimeMillis()-initiallStallMillis) < maxMillis ) {
            try {
                if ( f.get() ) {
                    return;
                }
            } catch ( Throwable ex ) {
                exceptionCount += 1;

                if ( firstException == null ) {
                    firstException = ex;
                }
            }
        }

        if ( firstException == null ) {
            Assertions.fail( errorMessage.get() );
        } else {
            Assertions.fail( errorMessage.get() +"; " + exceptionCount + " exceptions occurred.  The first exception was: '" + firstException.getMessage() + "' and is chained below", firstException);
        }
    }

    /**
     * Repeatedly sample the specified value until the value stops changing.  The function must
     * return the same value three times in a row before being considered a success.  Each call
     * is at least 100 milliseconds apart.
     */
    public static <T> void spinUntilStable( Supplier<T> sampleFunction ) {
        spinUntilStable( sampleFunction, 100 );
    }

    /**
     * Repeatedly sample the specified value until the value stops changing.  The function must
     * return the same value three times in a row before being considered a success.  Each call
     * is at least `pollFrequencyMillis` milliseconds apart.
     */
    public static <T> void spinUntilStable( Supplier<T> sampleFunction, long pollFrequencyMillis ) {
        spinUntilStable( sampleFunction, pollFrequencyMillis, 60_000 );
    }

    /**
     * Repeatedly sample the specified value until the value stops changing.  The function must
     * return the same value three times in a row before being considered a success.  Each call
     * is at least `pollFrequencyMillis` milliseconds apart.
     */
    public static <T> void spinUntilStable( Supplier<T> sampleFunction, long pollFrequencyMillis, long maxDurationMillis ) {
        AtomicReference<T> lastValue = new AtomicReference<>( sampleFunction.get() );
        AtomicInteger      counter   = new AtomicInteger();

        spinUntilTrue(
            () -> {
                sleep(pollFrequencyMillis);

                T soFar = sampleFunction.get();
                if ( Objects.equals(soFar, lastValue.get()) ) {
                    if ( counter.incrementAndGet() >= 2 ) {
                        return true;
                    }
                } else {
                    counter.set( 0 );
                    lastValue.set( soFar );
                }

                return false;
            },
            Duration.ofMillis(maxDurationMillis)
        );
    }

    /**
     * Repeatedly calls the function f until it returns false.  If the method fails to return true
     * within the supplied time period then the assertion will fail.
     */
    public static void spinUntilFalse( Supplier<Boolean> f ) {
        spinUntilTrue( () -> !f.get() );
    }

    public static void awaitLatch( CountDownLatch latch ) {
        awaitLatch( latch, DEFAULT_TIMEOUT.toMillis() );
    }

    public static void awaitLatch( CountDownLatch latch, long numMillis ) {
        try {
            assertTrue( latch.await(numMillis, TimeUnit.MILLISECONDS) );
        } catch ( InterruptedException e ) {
            throw new RuntimeException(e);
        }
    }

    public static void assertThrows( Class<?> expectedExceptionClass, String expectedExceptionMessage, RunnableWithThrows codeToTest ) {
        try {
            codeToTest.run();
        } catch ( AssertionFailedError ex ) {
            throw ex;
        } catch ( Throwable ex ) {
            assertTrue( expectedExceptionClass.isAssignableFrom(ex.getClass()), "Caught "+ex.getClass().getName() + " when expecting " + expectedExceptionClass.getName() );
            assertEquals( expectedExceptionMessage, ex.getMessage() );
        }
    }

    private static void sleep( long millis ) {
        try {
            Thread.sleep( millis );
        } catch ( InterruptedException e ) {
        }
    }
}
