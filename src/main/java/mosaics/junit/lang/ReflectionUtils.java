package mosaics.junit.lang;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Nested;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;


@SuppressWarnings( "unchecked" )
public class ReflectionUtils {

    public static Stream<Field> allFieldsFor( Class c ) {
        return walkInheritanceTreeFor(c).flatMap( k -> Arrays.stream(k.getDeclaredFields()) );
    }

    public static Stream<Class> walkInheritanceTreeFor( Class c ) {
        return iterate( c, k -> Optional.ofNullable(k.getSuperclass()) );
    }

    public static <T> Stream<T> iterate( T seed, Function<T,Optional<T>> fetchNextFunction ) {
        Objects.requireNonNull(fetchNextFunction);

        Iterator<T> iterator = new Iterator<T>() {
            private Optional<T> t = Optional.ofNullable(seed);

            public boolean hasNext() {
                return t.isPresent();
            }

            public T next() {
                T v = t.get();

                t = fetchNextFunction.apply(v);

                return v;
            }
        };

        return StreamSupport.stream(
            Spliterators.spliteratorUnknownSize( iterator, Spliterator.ORDERED | Spliterator.IMMUTABLE),
            false
        );
    }

    public static void setField( Object instance, Field field, Object v ) {
        field.setAccessible( true );

        try {
            field.set( instance, v );
        } catch ( IllegalAccessException e ) {
            throw new RuntimeException( e );
        }
    }

    public static Object getField( Object instance, String fieldName ) {
        try {
            Field f = instance.getClass().getDeclaredField( fieldName );

            return getField( instance, f );
        } catch ( NoSuchFieldException e ) {
            throw new RuntimeException( e );
        }
    }

    @SneakyThrows
    public static Object getField( Object instance, Field field ) {
        field.setAccessible( true );

        return field.get( instance );
    }

//    public static ParameterMeta uneraseParameter( Parameter parameter ) {
//        Method              owningMethod = (Method) parameter.getDeclaringExecutable();
//        Class<?>            owningClass  = owningMethod.getDeclaringClass();
//        Optional<ClassMeta> classMetaOpt = ClassMetaLoader.loadClassMetaFor( owningClass );
//
//        Optional<ParameterMeta> parameterMetaOpt = classMetaOpt
//            .flatMap( cm -> cm.getMethodMetaFor(owningMethod) )
//            .flatMap( mm -> mm.getParameterMetaFor(parameter) );
//
//        return parameterMetaOpt.orElseGet( () -> new ParameterMeta(parameter.getType(),parameter.getName()) );
//    }

    public static boolean isNestedJUnitTestClass( Object tc ) {
        return hasAnnotation( tc, Nested.class ) && isAnonymousInnerClass( tc );
    }

    private static boolean isAnonymousInnerClass( Object tc ) {
        return tc != null && tc.getClass().getEnclosingClass() != null;
//        try {
//            return tc != null && tc.getClass().getDeclaredField("this$0") != null;
//        } catch ( NoSuchFieldException e ) {
//            return false;
//        }
    }

    private static boolean hasAnnotation( Object tc, Class annotationType ) {
        return tc.getClass().getAnnotation(annotationType) != null;
    }

    public static Object getContainingObjectForAnonymousInnerClass( Object tc ) {
        int depthCount = countEnclosingClassDepth(tc.getClass());
        return getField( tc, "this$"+(depthCount-1) );
    }

    private static int countEnclosingClassDepth( Class<?> c ) {
        int depthCount = 0;

        while (c.getEnclosingClass() != null ) {
            depthCount += 1;

            c = c.getEnclosingClass();
        }

        return depthCount;
    }

    /**
     * Can a be casted to b?
     *
     * @param a The type of the object in question
     * @param b The type of the interface that we are checking
     */
    public static <R, T> boolean isCastableTo( Class<R> a, Class<T> b ) {
        return b.isAssignableFrom( a );
    }

    public static boolean isCastableTo( Type a, Type b ) {
        return toClass(b).isAssignableFrom( toClass(a) );
    }

    public static <T> Class<T> toClass( Type type ) {
        if ( type instanceof Class ) {
            return (Class<T>) type;
        } else if ( type instanceof ParameterizedType t ) {
            return toClass( t.getRawType() );
        }

        try {
            return (Class<T>) Class.forName( type.getTypeName() );
        } catch ( ClassNotFoundException e ) {
            throw new IllegalArgumentException( type + " cannot be converted to a Class", e );
        }
    }

    private static final Map<Type,Object> DEFAULT_VALUES = Map.of(
        boolean.class, false,
        byte.class, (byte) 0,
        char.class, (char) 0,
        short.class, (short) 0,
        int.class, 0,
        long.class, 0L,
        float.class, 0f,
        double.class, 0d
    );

    private static final Set<Object> DEFAULT_FIELD_VALUES = Set.copyOf(DEFAULT_VALUES.values());

    public static boolean isUnset( Object obj, Field f ) {
        Object currentFieldValue = getField( obj, f );

        return currentFieldValue == null || DEFAULT_FIELD_VALUES.contains( currentFieldValue );
    }

    public static void clearField( Field f, Object o ) {
        setField( o, f, DEFAULT_VALUES.get(f.getType()) );
    }

    public static StackWalker.StackFrame getCallersStackFrame() {
        StackWalker stackWalker = StackWalker.getInstance(RETAIN_CLASS_REFERENCE);
        return stackWalker.walk(
            frames ->
                frames
                    .skip(2)
                    .findFirst()
                    .get()
        );
    }

    public static boolean isStatic( Field field ) {
        return Modifier.isStatic( field.getModifiers() );
    }

    public static boolean isInstanceField( Field field ) {
        return !isStatic( field );
    }

    public static boolean isNotSyntheticField( Field field ) {
        return !field.isSynthetic();
    }

    public static boolean isBoolean( Type type ) {
        return isBoolean( toClass(type) );
    }

    public static boolean isBoolean( Class type ) {
        return type == boolean.class || type == Boolean.class;
    }

    public static boolean isByte( Type type ) {
        return isByte( toClass(type) );
    }

    public static boolean isByte( Class type ) {
        return type == byte.class || type == Byte.class;
    }

    public static boolean isShort( Type type ) {
        return isShort( toClass(type) );
    }

    public static boolean isShort( Class type ) {
        return type == short.class || type == Short.class;
    }

    public static boolean isInteger( Type type ) {
        return isInteger( toClass(type) );
    }

    public static boolean isInteger( Class type ) {
        return type == int.class || type == Integer.class;
    }

    public static boolean isLong( Type type ) {
        return isLong( toClass(type) );
    }

    public static boolean isLong( Class type ) {
        return type == long.class || type == Long.class;
    }

    public static boolean isCharacter( Type type ) {
        return isCharacter( toClass(type) );
    }

    public static boolean isCharacter( Class type ) {
        return type == char.class || type == Character.class;
    }

    public static boolean isFloat( Type type ) {
        return isFloat( toClass(type) );
    }

    public static boolean isFloat( Class type ) {
        return type == float.class || type == Float.class;
    }

    public static boolean isDouble( Type type ) {
        return isDouble( toClass(type) );
    }

    public static boolean isDouble( Class type ) {
        return type == double.class || type == Double.class;
    }

    public static <T> Object newArray(Class componentType, int arrayLength, Supplier<T> factory) {
        Object array = Array.newInstance(componentType, arrayLength);

        for ( int i=0; i<arrayLength; i++ ) {
            Object newValue = factory.get();

            Array.set( array, i, newValue );
        }

        return array;
    }

    public static List<Type> getTypeParameters( Type type ) {
        if ( type instanceof ParameterizedType t ) {
            return Arrays.asList(t.getActualTypeArguments());
        } else {
            return Collections.emptyList();
        }
    }

    public static boolean isArray( Type type ) {
        return toClass(type).isArray();
    }

    public static boolean isEnum( Type type ) {
        return toClass(type).isEnum();
    }

    public static boolean isList( Type type ) {
        return isCastableTo( type, List.class );
    }

    public static boolean isSet( Type type ) {
        return isCastableTo( type, Set.class );
    }

    public static boolean isFunction( Type type ) {
        return isCastableTo( type, Function.class );
    }

    public static boolean isMap( Type type ) {
        return isCastableTo( type, Map.class );
    }
}
