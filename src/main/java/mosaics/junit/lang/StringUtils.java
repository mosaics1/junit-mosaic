package mosaics.junit.lang;

import java.util.Arrays;

public class StringUtils {
    public static String[] splitLine( String line ) {
        String[] lines = line.split("\n");

        return Arrays.stream( lines )
                .map(l -> l.trim().startsWith("|") ? l.substring(l.indexOf('|')+1) : l)
                .toArray(String[]::new);
    }
}
