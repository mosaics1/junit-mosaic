package mosaics.junit.lang;

import java.util.function.Consumer;


public interface ConsumerWithThrows<T> {
    void accept(T t) throws Throwable;

    default Consumer<T> toConsumer() {
        return v -> {
            try {
                this.accept( v );
            } catch ( Throwable ex ) {
                throw new RuntimeException( ex );
            }
        };
    }
}
