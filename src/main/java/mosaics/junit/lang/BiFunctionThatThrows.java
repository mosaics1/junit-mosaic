package mosaics.junit.lang;

import java.util.function.BiFunction;


public interface BiFunctionThatThrows<T,U,R> {
    public R apply(T t, U u) throws Throwable;

    public default BiFunction<T,U,R> toBiFunction() {
        BiFunctionThatThrows<T,U,R> orig = this;

        return (a,b) -> {
            try {
                return orig.apply( a, b );
            } catch ( Throwable ex ) {
                throw new RuntimeException( ex );
            }
        };
    }
}
