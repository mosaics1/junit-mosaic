package mosaics.junit.lang;

import java.util.function.Supplier;


public interface SupplierWithThrows<T> {
    public T get() throws Throwable;

    public default Supplier<T> toSupplier() {
        return () -> {
            try {
                return this.get();
            } catch ( Throwable ex ) {
                throw new RuntimeException( ex );
            }
        };
    }
}
