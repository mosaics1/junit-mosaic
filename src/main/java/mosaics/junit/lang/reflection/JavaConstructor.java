package mosaics.junit.lang.reflection;

import lombok.SneakyThrows;
import mosaics.junit.lang.ListUtils;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;


public class JavaConstructor implements JavaExecutable {
    private Constructor            constructor;
    private Map<String, JavaClass> generics;

    JavaConstructor( Constructor constructor) {
        this(constructor, Map.of());
    }

     JavaConstructor( Constructor constructor, Map<String, JavaClass> generics ) {
         this.constructor = constructor;
         this.generics    = generics;
     }

    public List<JavaParameter> getParameters() {
        return ListUtils.map(constructor.getParameters(), p -> JavaParameter.of(p,generics));
    }

    public int getModifiers() {
        return constructor.getModifiers();
    }

    public int getParameterCount() {
        return constructor.getParameterCount();
    }

    @SneakyThrows
    public Object newInstance( Object[] args ) {
        return constructor.newInstance( args );
    }
}
