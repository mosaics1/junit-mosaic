package mosaics.junit.lang.reflection;

import lombok.SneakyThrows;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.Optional;


public class JavaField {
    public static Optional<JavaField> of(Class c, String fieldName) {
        return JavaClass.of(c).getField( fieldName );
    }

    static JavaField of(Field field) {
        return of(field, Map.of());
    }

    static JavaField of(Field field, Map<String,JavaClass> reifiedGenerics) {
        JavaClass type = JavaClass.of( field.getGenericType(), reifiedGenerics );

        return new JavaField( type, field );
    }

    private JavaClass fieldType;
    private Field     field;

    JavaField( JavaClass fieldType, Field field ) {
        this.fieldType = fieldType;
        this.field     = field;
    }

    public JavaClass getType() {
        return fieldType;
    }

    public String getName() {
        return field.getName();
    }

    @SneakyThrows
    public <T> T getValue(Class<T> expectedType) {
        field.setAccessible( true );

        return (T) field.get(null);
    }

    @SneakyThrows
    public void clear() {
        field.setAccessible( true );
        field.set(null, null);
    }

    public boolean isStatic() {
        return Modifier.isStatic( field.getModifiers() );
    }
}
