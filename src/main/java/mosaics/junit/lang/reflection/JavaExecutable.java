package mosaics.junit.lang.reflection;

import java.lang.reflect.Modifier;
import java.util.List;


public interface JavaExecutable {
    public List<JavaParameter> getParameters();
    public int getModifiers();
    public int getParameterCount();

    public default boolean isStatic() {
        return Modifier.isStatic(getModifiers());
    }

    public default boolean isPublic() {
        return Modifier.isPublic(getModifiers());
    }
}
