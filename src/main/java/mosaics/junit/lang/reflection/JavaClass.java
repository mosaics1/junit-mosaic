package mosaics.junit.lang.reflection;

import mosaics.junit.lang.ReflectionUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public interface JavaClass {
    public static JavaClass of( Class t ) {
        return new UnparameterisedJavaClass(t);
    }

    public static JavaClass of( ParameterizedType t ) {
        return new ParameterisedJavaClass(t);
    }

    public static JavaClass of( Type type ) {
        return of( type, Map.of() );
    }

    public static JavaClass of( Class type, Class...typeParameters ) {
        if (typeParameters.length == 0) {
            return of(type);
        } else {
            return of(
                new ParameterizedType() {
                    public Type[] getActualTypeArguments() {
                        return typeParameters;
                    }

                    public Type getRawType() {
                        return type;
                    }

                    public Type getOwnerType() {
                        return null;
                    }
                }
            );
        }
    }

    public static JavaClass of( Type type, Map<String,JavaClass> reifiedGenerics ) {
        if ( type instanceof ParameterizedType t ) {
            return of(t);
        } else if ( type instanceof Class t ) {
            return of( t );
        } else if ( type instanceof TypeVariable t ) {
            return Optional.ofNullable( reifiedGenerics.get(t.getName()) )
                .orElseThrow(() -> new IllegalArgumentException("Unknown generic type reference "+type+", known generics are: "+reifiedGenerics) );
        } else {
            throw new UnsupportedOperationException("Unknown type "+type.getClass()+" ("+type+")");
        }
    }

    public Class getJdkClass();
    public List<JavaClass> getClassGenerics();
    public Optional<JavaField> getField( String fieldName );
    public List<JavaConstructor> getConstructors();
    public List<JavaMethod> getMethods();


    public default boolean isEnum() {
        return ReflectionUtils.isEnum( getJdkClass() );
    }

    public default boolean isBoolean() {
        return ReflectionUtils.isBoolean( getJdkClass() );
    }

    public default boolean isByte() {
        return ReflectionUtils.isByte( getJdkClass() );
    }

    public default boolean isShort() {
        return ReflectionUtils.isShort( getJdkClass() );
    }

    public default boolean isInteger() {
        return ReflectionUtils.isInteger( getJdkClass() );
    }

    public default boolean isLong() {
        return ReflectionUtils.isLong( getJdkClass() );
    }

    public default boolean isFloat() {
        return ReflectionUtils.isFloat( getJdkClass() );
    }

    public default boolean isDouble() {
        return ReflectionUtils.isDouble( getJdkClass() );
    }

    public default boolean isCharacter() {
        return ReflectionUtils.isCharacter( getJdkClass() );
    }

    public default boolean isArray() {
        return ReflectionUtils.isArray( getJdkClass() );
    }

    public default boolean isSet() {
        return ReflectionUtils.isSet( getJdkClass() );
    }

    public default boolean isFunction() {
        return ReflectionUtils.isFunction( getJdkClass() );
    }

    public default boolean isList() {
        return ReflectionUtils.isList( getJdkClass() );
    }

    public default boolean isMap() {
        return ReflectionUtils.isMap( getJdkClass() );
    }

    public default Optional<JavaMethod> getMethod( String methodName, Class...argTypes ) {
        return getMethods().stream()
            .filter( m -> m.getName().equals(methodName) )
            .filter( m -> m.getParameters().size() == argTypes.length )
            .filter( m -> Objects.equals(toBaseClassesOnly(m.getParameters()), Arrays.asList(argTypes)) )
            .findFirst();
    }

    private static List<Class> toBaseClassesOnly( List<JavaParameter> parameters ) {
        return parameters.stream()
            .map( p -> p.type().getJdkClass() )
            .collect( Collectors.toList() );
    }

    public default JavaClass getComponentType() {
        return JavaClass.of( getJdkClass().getComponentType() ); // NB arrays do not support generics
    }

    public default Object newArray( int arrayLength, Supplier componentFactory ) {
        return ReflectionUtils.newArray( getJdkClass(), arrayLength, componentFactory );
    }

    public default <T extends Annotation> T getAnnotation( Class<T> type ) {
        return (T) getJdkClass().getAnnotation( type );
    }
}
