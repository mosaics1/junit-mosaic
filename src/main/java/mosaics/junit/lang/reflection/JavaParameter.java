package mosaics.junit.lang.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;


public record JavaParameter(Optional<JavaClass>declaredAt, JavaClass type, String name, List<Annotation> annotations) {
    public static JavaParameter of( Class c ) {
        JavaClass        type        = JavaClass.of( c );
        List<Annotation> annotations = Arrays.asList();

        return new JavaParameter(Optional.empty(), type, null, annotations);
    }

    public static JavaParameter of( JavaClass type ) {
        List<Annotation> annotations = Arrays.asList();

        return new JavaParameter(Optional.empty(), type, null, annotations);
    }

    public static JavaParameter of( Field f ) {
        JavaClass        type        = JavaClass.of( f.getGenericType() );
        List<Annotation> annotations = Arrays.asList( f.getAnnotations() );

        return new JavaParameter(Optional.of(JavaClass.of(f.getDeclaringClass())), type, f.getName(), annotations);
    }

    public static JavaParameter of( Parameter p, Map<String, JavaClass> generics ) {
        return new JavaParameter(
            Optional.of(JavaClass.of(p.getDeclaringExecutable().getDeclaringClass())),
            JavaClass.of(p.getParameterizedType(),generics), p.getName(), Arrays.asList(p.getAnnotations())
        );
    }

    public <A extends Annotation> boolean hasAnnotation( Class<A> annotationClass ) {
        return annotations.stream().anyMatch( a -> a.annotationType() == annotationClass );
    }

    public <A extends Annotation> Optional<A> getAnnotation( Class<A> annotationClass ) {
        return annotations.stream()
            .filter( a -> a.annotationType() == annotationClass )
            .map( a -> (A) a )
            .findFirst();
    }
}
