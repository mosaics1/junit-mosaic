package mosaics.junit.lang.reflection;

import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import mosaics.junit.lang.ListUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


@EqualsAndHashCode
public class UnparameterisedJavaClass implements JavaClass {
    private Class type;

    UnparameterisedJavaClass( Class type ) {
        this.type = type;
    }


    public Class getJdkClass() {
        return type;
    }

    public List<JavaClass> getClassGenerics() {
        return Collections.emptyList();
    }

    @SneakyThrows
    public Optional<JavaField> getField( String fieldName ) {
        try {
            return Optional.ofNullable( type.getDeclaredField( fieldName ) ).map( JavaField::of );
        } catch ( NoSuchFieldException ex ) {
            return Optional.empty();
        }
    }

    public List<JavaConstructor> getConstructors() {
        return ListUtils.map(type.getConstructors(), JavaConstructor::new);
    }

    public List<JavaMethod> getMethods() {
        return ListUtils.map(type.getMethods(), JavaMethod::new);
    }

    public String toString() {
        return type.getName();
    }
}
