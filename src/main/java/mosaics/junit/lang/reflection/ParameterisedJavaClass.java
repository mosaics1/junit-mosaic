package mosaics.junit.lang.reflection;

import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import mosaics.junit.lang.ListUtils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static mosaics.junit.lang.ReflectionUtils.toClass;


@EqualsAndHashCode
public class ParameterisedJavaClass implements JavaClass {
    private Class                  clazz;
    private Map<String, JavaClass> generics;

    ParameterisedJavaClass( ParameterizedType type ) {
        this(toClass(type), extractGenericsFrom(type));
    }

    ParameterisedJavaClass( Class clazz, Map<String, JavaClass> generics ) {
        this.clazz    = clazz;
        this.generics = generics;
    }

    public Class getJdkClass() {
        return clazz;
    }

    public List<JavaClass> getClassGenerics() {
        List list = new ArrayList<>();

        for ( TypeVariable v : clazz.getTypeParameters() ) {
            list.add( generics.get(v.getName()) );
        }

        return list;
    }

    @SneakyThrows
    @SuppressWarnings("ConstantConditions")
    public Optional<JavaField> getField( String fieldName ) {
        return Optional.ofNullable(clazz.getDeclaredField(fieldName))
            .map( field -> JavaField.of(field,generics) );
    }

    public List<JavaConstructor> getConstructors() {
        return ListUtils.map(clazz.getConstructors(), constructor -> new JavaConstructor(constructor,generics));
    }

    public List<JavaMethod> getMethods() {
        return ListUtils.map(clazz.getMethods(), constructor -> new JavaMethod(constructor,generics));
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();

        buf.append(clazz.getName());
        buf.append('<');

        String separator = "";
        for ( TypeVariable v : clazz.getTypeParameters() ) {
            buf.append( separator );
            buf.append( generics.get(v.getName()) );

            separator = ", ";
        }

        buf.append('>');

        return buf.toString();
    }

    @SuppressWarnings("rawtypes")
    private static Map<String, JavaClass> extractGenericsFrom( ParameterizedType type ) {
        Class          clazz         = toClass(type);
        Type[]         generics      = type.getActualTypeArguments();
        TypeVariable[] typeVariables = clazz.getTypeParameters();

        Map<String,JavaClass> extractedGenerics = new HashMap<>();

        for ( int i=0; i<typeVariables.length; i++ ) {
            extractedGenerics.put( typeVariables[i].getTypeName(), JavaClass.of(generics[i]) );
        }

        return extractedGenerics;
    }
}
