package mosaics.junit.lang.reflection;

import lombok.SneakyThrows;
import mosaics.junit.lang.ListUtils;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;


public class JavaMethod implements JavaExecutable {
    private Method                 method;
    private Map<String, JavaClass> generics;

    JavaMethod( Method method ) {
        this( method, Map.of());
    }

    JavaMethod( Method method, Map<String, JavaClass> generics ) {
        this.method   = method;
        this.generics = generics;
    }

    public String getName() {
        return method.getName();
    }

    public List<JavaParameter> getParameters() {
        return ListUtils.map(method.getParameters(), p -> JavaParameter.of(p,generics));
    }

    public int getParameterCount() {
        return method.getParameterCount();
    }

    @SneakyThrows
    public Object invoke( Object obj, Object[] args ) {
        return method.invoke( obj, args );
    }

    public int getModifiers() {
        return method.getModifiers();
    }
}
