package mosaics.junit.lang;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class ListUtils {
    public static <A,B> List<B> map( A[] array, Function<A,B> op ) {
        return Arrays.stream(array)
            .map(op::apply)
            .collect( Collectors.toList());
    }
}
