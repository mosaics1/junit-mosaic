package mosaics.junit.lang;

public interface RunnableWithThrows {
    public void run() throws Throwable;

    public default Runnable toRunnable() {
        RunnableWithThrows orig = this;

        return () -> {
            try {
                orig.run();
            } catch ( Throwable ex ) {
                throw new RuntimeException( ex );
            }
        };
    }
}
