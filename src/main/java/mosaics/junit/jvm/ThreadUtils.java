package mosaics.junit.jvm;

import java.util.Set;
import java.util.stream.Collectors;


public class ThreadUtils {
    public static Set<ThreadIdentifier> captureActiveThreads() {
        return Thread.getAllStackTraces().keySet().stream()
            .map( ThreadIdentifier::new )
            .collect( Collectors.toSet());
    }
}
