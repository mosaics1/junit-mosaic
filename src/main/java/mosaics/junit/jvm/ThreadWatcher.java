package mosaics.junit.jvm;


import lombok.SneakyThrows;
import mosaics.junit.JMAssertions;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Detects when a block of code fails to shutdown a thread that it started.  The ThreadWatcher works
 * by taking a snapshot of active threads before and after a critical region.  The two snapshots
 * are compared, and raises an error when any threads remain. <p/>
 *
 * The critical region is controlled by a semaphore. The semaphore ensures that only one thread may enter the
 * critical region at a time which reduces the chance of getting noise from running different tests
 * together.<p/>
 *
 * Some threads are designed to stay running.  This some threads may be added to the ThreadWatcher
 * to be ignored and any that is annotated with @SystemThread will also be ignored.
 */
public class ThreadWatcher {

    private final Supplier<Set<ThreadIdentifier>> captureActiveThreadsF;
    private final Semaphore                       criticalRegionSemaphore  = new Semaphore( 1 );
    private final Set<Class>                      excludedThreadClasses    = Set.of(PauseDetecter.PauseDetectorThread.class);
    private final Set<Predicate<String>>          excludedThreadPredicates = new HashSet<>();

    private Set<ThreadIdentifier> beforeThreads;


    public ThreadWatcher() {
        this( ThreadUtils::captureActiveThreads );
    }
    
    public ThreadWatcher( Supplier<Set<ThreadIdentifier>> captureActiveThreadsF ) {
        this.captureActiveThreadsF = captureActiveThreadsF;
    }

    /**
     * Register a predicate that will be used to skip active threads by thread name
     * and their fully qualified class name.
     *
     * @Parameter predicate skip if returns true
     */
    public ThreadWatcher ignore( Predicate<String> predicate ) {
        excludedThreadPredicates.add( predicate );

        return this;
    }

    public ThreadWatcher ignore( String pattern ) {
        return ignore( Pattern.compile(pattern).asPredicate() );
    }

    @SneakyThrows
    public void startThreadCleanupCriticalRegion() {
        criticalRegionSemaphore.acquire();

        beforeThreads = captureAndFilterActiveThreads( excludedThreadClasses );
    }

    public void exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning() {
        try {
            Set<ThreadIdentifier> afterThreads = captureAndFilterActiveThreads( excludedThreadClasses );

            afterThreads.removeAll( beforeThreads );

            spinUntilAllTestCreatedThreadsHaveStopped( afterThreads );
        } finally {
            beforeThreads.clear();
            criticalRegionSemaphore.release();
        }
    }

    private void spinUntilAllTestCreatedThreadsHaveStopped( Set<ThreadIdentifier> afterThreads ) {
        JMAssertions.spinUntilTrue(
            () -> {
                afterThreads.removeIf( ThreadIdentifier::hasStopped );

                return afterThreads.isEmpty();
            },
            () -> createNaughtyThreadErrorMessage(afterThreads)
        );
    }

    private static String createNaughtyThreadErrorMessage( Set<ThreadIdentifier> naughtyThreads ) {
        StringBuilder buf = new StringBuilder();

        buf.append( naughtyThreads.size() );
        buf.append( " thread" );

        if ( naughtyThreads.size() > 1 ) {
            buf.append( "s are" );
        } else {
            buf.append( " is" );
        }

        buf.append( " still running after the test completed: " );
        buf.append( System.lineSeparator() );

        for ( ThreadIdentifier thread : naughtyThreads ) {
            buf.append( "    " );
            buf.append( thread );
            buf.append( System.lineSeparator() );
        }

        return buf.toString();
    }

    public Set<ThreadIdentifier> captureAndFilterActiveThreads( Set<Class> excludeSet ) {
        return captureActiveThreadsF.get().stream()
            .filter( threadIdentifier -> !excludeSet.contains(threadIdentifier.getThreadClass()) )
            .filter( threadIdentifier -> !isDeclaredAsASystemThread( threadIdentifier.getThreadClass() ) )
            .filter( threadIdentifier -> !matchesAnExclusionPattern(threadIdentifier) )
            .collect( Collectors.toSet());
    }

    private boolean matchesAnExclusionPattern( ThreadIdentifier threadIdentifier ) {
        String threadName      = threadIdentifier.getThreadName();
        String threadClassName = threadIdentifier.getThreadClass().getName();

        return excludedThreadPredicates.stream().anyMatch(
            predicate -> predicate.test(threadName) || predicate.test(threadClassName)
        );
    }

    @SuppressWarnings("unchecked")
    private static boolean isDeclaredAsASystemThread( Class threadClass ) {
        // todo consider filtering out internal jdk packages
        try {
            Class systemThreadAnnotationClass = Class.forName( "com.softwaremosaic.lang.annotations.SystemThread" );

            return threadClass.getAnnotation(systemThreadAnnotationClass) != null;
        } catch ( ClassNotFoundException e ) {
            return false;
        }
    }
}
