package mosaics.junit.jvm;


import java.util.concurrent.atomic.DoubleAdder;


/**
 * This tool estimates how much time has been lost to JVM freezes.
 */
public class PauseDetecter {

    private static PauseDetectorThread INSTANCE = new PauseDetectorThread();

    static {
        INSTANCE.start();
    }

    public static long getTotalStallTimeMillis() {
        return INSTANCE.totalStallTimeMillisRef.longValue();
    }


    public static void main( String[] args ) throws InterruptedException {
        new PauseDetectorThread().start();

        Thread.sleep(1000 * 60 );
    }

    public static class PauseDetectorThread extends Thread {
        private final long waitMillis = 1000;

        private final DoubleAdder totalStallTimeMillisRef = new DoubleAdder();


        public PauseDetectorThread() {
            setDaemon( true );
            setName( "PauseDetectorThread" );

            setPriority( Thread.MAX_PRIORITY );
        }

        public void run() {
            super.run();

            while ( true ) {
                totalStallTimeMillisRef.add( timedSleep(waitMillis) );
            }
        }

        private double timedSleep( long targetSleepMillis ) {
            long t0Nanos = System.nanoTime();
            try {
                Thread.sleep( targetSleepMillis );

                long t1Nanos        = System.nanoTime();
                long durationNanos  = t1Nanos - t0Nanos;

                return Math.max(0.0, (durationNanos / 1_000_000.0) - targetSleepMillis);
            } catch ( InterruptedException e ) {
                return 0;  // interrupted - ruins the timing so don't try
            }
        }
    }
}
