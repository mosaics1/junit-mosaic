package mosaics.junit.jvm;


import lombok.SneakyThrows;
import mosaics.junit.JMAssertions;

import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.StackWalker.Option.RETAIN_CLASS_REFERENCE;


/**
 * Tool for blocking a thread until all registered objects have been claimed by the JVM's
 * Garbage Collector.
 */
public class GCBarrier {
    private final Map<Reference<Object>, AllocationRecord> allocationDetails = new ConcurrentHashMap<>();
    private final ReferenceQueue<Object>                   referenceQueue    = new ReferenceQueue<>();


    /**
     * Register the specified object so that this watch will be notified of the objects death.
     */
    public void push( Object object ) {
        StackWalker stackWalker = StackWalker.getInstance(RETAIN_CLASS_REFERENCE);

        StackWalker.StackFrame callerFrame = stackWalker.walk(
            frames ->
                frames
                    .dropWhile( f -> f.getClassName().equals(this.getClass().getName()))
                    .findFirst().get()
        );

        push( callerFrame, object );
    }

    /**
     * Register the specified object so that this watch will be notified of the objects death.
     */
    public void push( Method allocationMethod, Object object) {
        push( allocationMethod.getDeclaringClass().getName(), allocationMethod.getName(), object );
    }

    /**
     * Register the specified object so that this watch will be notified of the objects death.
     */
    public void push(StackWalker.StackFrame allocationFrame, Object object) {
        push( allocationFrame.getClassName(), allocationFrame.getMethodName(), object );
    }

    /**
     * Register the specified object so that this watch will be notified of the objects death.
     */
    public synchronized void push(String sourceClass, String sourceMethod, Object object) {
        PhantomReference<Object> ref = new PhantomReference<>( object, referenceQueue );

        allocationDetails.put( ref, new AllocationRecord(sourceClass, sourceMethod, object.getClass()) );
    }

    /**
     * Errors if all of the objects registered with this class have not been reclaimed by the garbage
     * collector within JMAssertions.DEFAULT_TIMEOUT.
     */
    public void spinUntilAllObjectsHaveBeenReleased() {
        spinUntilAllObjectsHaveBeenReleased(JMAssertions.DEFAULT_TIMEOUT );
    }

    /**
     * Errors if all of the objects registered with this class have not been reclaimed by the garbage
     * collector within the specified period of time.
     */
    public synchronized void spinUntilAllObjectsHaveBeenReleased( Duration maxWait ) {
        JMAssertions.spinUntilTrue( this::pollReferenceQueue, maxWait, this::generateErrorMessage );
    }

    @SneakyThrows
    private boolean pollReferenceQueue() {
        Thread.sleep(100);
        System.gc();

        Reference<?> ref = referenceQueue.poll();

        if ( ref != null ) {
            allocationDetails.remove( ref );
        }

        return haveAllObjectsBeenReleased();
    }

    private String generateErrorMessage() {
        StringBuilder buf = new StringBuilder();

        buf.append(allocationDetails.size() + " object(s) have not been released"+System.lineSeparator());

        for ( AllocationRecord rec : allocationDetails.values() ) {
            buf.append( "    an instance of "+rec.type()+" was allocated at "+rec.className()+"::"+rec.methodName()+System.lineSeparator());
        }

        return buf.toString();
    }

    private boolean haveAllObjectsBeenReleased() {
        return allocationDetails.isEmpty();
    }

    private static record AllocationRecord(String className, String methodName, Class<?> type) {}
}
