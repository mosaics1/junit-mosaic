package mosaics.junit.jvm;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Getter
@EqualsAndHashCode
public class ThreadIdentifier {
    private final long id;
    private final String threadName;
    private final Class threadClass;

    public ThreadIdentifier( Thread t ) {
        this( t.getId(), t.getName(), t.getClass() );
    }

    public ThreadIdentifier( long id, String threadName, Class threadClass ) {
        this.id          = id;
        this.threadName  = threadName;
        this.threadClass = threadClass;
    }

    public boolean isActive() {
        return getActiveThreadAndStackTrace().isPresent();
    }

    public boolean hasStopped() {
        return !isActive();
    }

    public List<StackTraceElement> getStackTrace() {
        return getActiveThreadAndStackTrace()
            .map( kv -> Arrays.asList(kv.getValue()) )
            .orElseGet( Collections::emptyList );
    }

    public Optional<Map.Entry<Thread,StackTraceElement[]>> getActiveThreadAndStackTrace() {
        return Thread.getAllStackTraces().entrySet().stream()
            .filter( kv -> isMatchingThread(kv.getKey()) )
            .findFirst();
    }

    private boolean isMatchingThread( Thread key ) {
        return key.getId() == this.id && key.getName().equals( this.threadName );
    }

    public String toString() {
        return "Thread(threadName="+threadName+", class="+threadClass.getName()+")";
    }
}
