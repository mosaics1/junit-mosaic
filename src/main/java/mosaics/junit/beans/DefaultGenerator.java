package mosaics.junit.beans;

import lombok.AllArgsConstructor;
import mosaics.junit.lang.reflection.JavaClass;

import java.util.function.Supplier;


@AllArgsConstructor
public class DefaultGenerator<T> implements Supplier<T> {
    private final JavaClass           type;
    private final RandomBeanGenerator randomBeanGenerator;
    private final String              fieldName;

    public T get() {
        return randomBeanGenerator.rnd( type, fieldName );
    }
}
