package mosaics.junit.beans;

import lombok.AllArgsConstructor;

import java.util.function.Supplier;


/**
 * Set of generators designed to be used with the @Random anotation.
 */
public class Generators {
    @AllArgsConstructor
    public static class FirstNameGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextFirstName();
        }
    }

    @AllArgsConstructor
    public static class FirstNameMaleGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextFirstNameMale();
        }
    }

    @AllArgsConstructor
    public static class FirstNameFemaleGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextFirstNameFemale();
        }
    }

    @AllArgsConstructor
    public static class LastNameGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextLastName();
        }
    }

    @AllArgsConstructor
    public static class CityGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextCity();
        }
    }

    @AllArgsConstructor
    public static class CountryGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextCountry();
        }
    }

    @AllArgsConstructor
    public static class EmailGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextEmail();
        }
    }

    @AllArgsConstructor
    public static class PhoneGenerator implements Supplier<String> {
        private final Random random;

        public String get() {
            return random.nextPhone();
        }
    }
}
