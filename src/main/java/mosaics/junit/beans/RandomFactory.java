package mosaics.junit.beans;

import java.util.function.Function;


/**
 * An interface for methods that know how to create objects of different types.
 */
public class RandomFactory {
    private Function<Class,Object> f;

    public RandomFactory(Function<Class,Object> f) {
        this.f = f;
    }

    public<T> T rnd( Class<T> type ) {
        return (T) f.apply( type );
    }
}
