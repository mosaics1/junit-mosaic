package mosaics.junit.beans;

import lombok.Getter;
import mosaics.junit.JMRandomExtension;
import mosaics.junit.lang.ReflectionUtils;
import mosaics.junit.lang.reflection.JavaClass;
import mosaics.junit.lang.reflection.JavaConstructor;
import mosaics.junit.lang.reflection.JavaExecutable;
import mosaics.junit.lang.reflection.JavaMethod;
import mosaics.junit.lang.reflection.JavaParameter;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;


@SuppressWarnings("unchecked")
public class RandomBeanGenerator {

    private static interface Factory<T> {
        public boolean supports( JavaClass type );
        public T create( JavaClass type, String name );
    }

    @Getter private Random        random;
            private List<Factory> factories;


    public RandomBeanGenerator() {
        this(System.nanoTime());
    }

    public RandomBeanGenerator( long seed ) {
        this( new Random(seed) );
    }

    // TODO how to support extra factories; especially ones from other libs
    //    todo document static method and default bean use
    public RandomBeanGenerator( Random random ) {
        this.random = random;

        this.factories = Arrays.asList(
            new BooleanFactory(random),
            new ByteFactory(random),
            new ShortFactory(random),
            new IntegerFactory(random),
            new LongFactory(random),
            new FloatFactory(random),
            new DoubleFactory(random),
            new CharacterFactory(random),
            new EnumFactory(random),
            new ListFactory(random, this),
            new SetFactory(random, this),
            new MapFactory(random, this),
            new OptionalFactory(random, this),
            new ArrayFactory(random, this),
            new StringFactory(random),
            new StaticMethodFactory(this),
            new DefaultBeanFactory(this)
        );
    }


    public boolean supports( JavaClass jc ) {
        return findFactoryFor(jc).isPresent();
    }

    public <T> T rnd( JavaParameter parameter ) {
        return (T) getSupplierFor(parameter).get();
    }

    public <T> T rnd( Class<T> type ) {
        return rnd(JavaClass.of(type),null);
    };

    private Supplier getSupplierFor( JavaParameter param ) {
//        Class paramType = param.type().getJdkClass();
        JMRandomExtension.Random rndAnnotation = param.getAnnotation( JMRandomExtension.Random.class ).orElse( null );
        if ( rndAnnotation == null ) {
            return new DefaultGenerator( param.type(), this, param.name() );
        }

        Class supplierType = rndAnnotation.generator();
        if ( !Supplier.class.isAssignableFrom(supplierType) ) {
            throw new IllegalArgumentException( "@Random(generator="+supplierType.getName()+") must be a class that implements Supplier");
        }

        return createSupplierFor( supplierType, param );
    }

    private Supplier createSupplierFor( Class supplierType, JavaParameter param ) {
        return Arrays.stream( supplierType.getConstructors() )
            .sorted( Comparator.comparing( Constructor::getParameterCount) )
            .flatMap( constructor -> createSupplierFor(constructor, param ) )
            .findFirst()
            .orElse( () -> null );
    }

    private Stream<Supplier> createSupplierFor( Constructor constructor, JavaParameter param ) {
        Object[] args = Arrays.stream( constructor.getParameterTypes() )
            .flatMap(constructorParameterType -> toValue(constructorParameterType,this,param))
            .toArray();

        try {
            return args.length == constructor.getParameterCount() ? Stream.ofNullable((Supplier)constructor.newInstance(args)) : Stream.empty();
        } catch ( Exception e ) {
            throw new RuntimeException(e);
        }
    }

    private Stream<Object> toValue( Class parameterType, RandomBeanGenerator generator, JavaParameter requestedDTOParameter ) {
        if ( parameterType == RandomBeanGenerator.class ) {
            return Stream.of( generator );
        } else if ( parameterType == mosaics.junit.beans.Random.class ) {
            return Stream.of( generator.getRandom() );
        } else if ( parameterType == JavaClass.class ) {
            return Stream.of(requestedDTOParameter.type());
        } else if ( parameterType == Class.class ) {
            return Stream.of(requestedDTOParameter.type().getJdkClass());
        } else if ( parameterType == String.class ) {
            return Stream.of(requestedDTOParameter.name());
        }

        return Stream.empty();
    }

    private <T> T rndInternal( Class<T> t ) {
        return rnd(t);
    }

    @SuppressWarnings("unchecked")
    public <T> T rnd( JavaClass type, String name ) {
        if ( type.isFunction() ) {
            Function<Class,Object> f = this::rndInternal;

            return (T) f;
        }

        return (T) findFactoryFor(type)
            .map( factory -> factory.create(type,name) )
            .orElseThrow( () -> new IllegalArgumentException("No factory found for "+type+" "+name) );
    }

    private <T> Optional<Factory<T>> findFactoryFor( JavaClass type ) {
        Optional first = factories.stream()
            .filter( f -> f.supports(type) )
            .findFirst();

        return (Optional<Factory<T>>) first;
    }



    private static class EnumFactory implements Factory<Enum> {
        private final Random random;

        private EnumFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isEnum();
        }

        public Enum create( JavaClass type, String name ) {
            Object[] enumValues = type.getJdkClass().getEnumConstants();

            int i = random.nextInt( 0, enumValues.length );
            return (Enum) enumValues[i];
        }
    }

    private static class BooleanFactory implements Factory<Boolean> {
        private final Random random;

        private BooleanFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isBoolean();
        }

        public Boolean create( JavaClass type, String name ) {
            return random.nextBoolean();
        }
    }

    private static class ByteFactory implements Factory<Byte> {
        private final Random random;

        private ByteFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isByte();
        }

        public Byte create( JavaClass type, String name ) {
            return random.nextByte();
        }
    }

    private static class ShortFactory implements Factory<Short> {
        private final Random random;

        private ShortFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isShort();
        }

        public Short create( JavaClass type, String name ) {
            return random.nextShort();
        }
    }

    private static class IntegerFactory implements Factory<Integer> {
        private final Random random;

        private IntegerFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isInteger();
        }

        public Integer create( JavaClass type, String name ) {
            return random.nextInt();
        }
    }

    private static class LongFactory implements Factory<Long> {
        private final Random random;

        private LongFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isLong();
        }

        public Long create( JavaClass type, String name ) {
            return random.nextLong();
        }
    }

    private static class FloatFactory implements Factory<Float> {
        private final Random random;

        private FloatFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isFloat();
        }

        public Float create( JavaClass type, String name ) {
            return random.nextFloat();
        }
    }

    private static class DoubleFactory implements Factory<Double> {
        private final Random random;

        private DoubleFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isDouble();
        }

        public Double create( JavaClass type, String name ) {
            return random.nextDouble();
        }
    }

    private static class CharacterFactory implements Factory<Character> {
        private final Random random;

        private CharacterFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return type.isCharacter();
        }

        public Character create( JavaClass type, String name ) {
            return random.nextChar();
        }
    }



    private static class OptionalFactory implements Factory<Optional> {
        private final Random              random;
        private final RandomBeanGenerator factory;

        private OptionalFactory( Random random, RandomBeanGenerator factory ) {
            this.random  = random;
            this.factory = factory;
        }

        public boolean supports( JavaClass type ) {
            return ReflectionUtils.isCastableTo(type.getJdkClass(), Optional.class);
        }

        public Optional create( JavaClass type, String name ) {
            List<JavaClass> typeParameters = type.getClassGenerics();
            if ( typeParameters.size() != 1 ) {
                return Optional.empty();
            }

            if ( random.nextBoolean() ) {
                return Optional.of(Optional.empty());
            } else {
                Object v = factory.rnd( typeParameters.get(0), name );

                return Optional.of(Optional.of(v));
            }
        }
    }

    private static class ArrayFactory implements Factory<Object> {
        private Random              random;
        private RandomBeanGenerator factory;

        private ArrayFactory( Random random, RandomBeanGenerator factory ) {
            this.random  = random;
            this.factory = factory;
        }

        public boolean supports( JavaClass type ) {
            return type.isArray();
        }

        public Object create( JavaClass type, String name ) {
            JavaClass componentType    = type.getComponentType();
            Supplier  componentFactory = factory.createSupplierFor(componentType,name);
            int       arrayLength      = random.nextInt( 0, 100 );

            return componentType.newArray(arrayLength, componentFactory);
        }
    }

    private Supplier createSupplierFor(JavaClass type, String name) {
        Factory factory = this.findFactoryFor(type).get();

        return () -> factory.create(type,name);
    }

    private static class ListFactory extends CollectionFactory<List,Object> {
        private ListFactory( Random random, RandomBeanGenerator rnd ) {
            super(
                random,
                rnd,
                type -> type.isList() && type.getClassGenerics().size() == 1,
                () -> new ArrayList<>(),
                type -> type.getClassGenerics().get( 0 ),
                List::add
            );
        }
    }

    private static class SetFactory extends CollectionFactory<Set,Object> {
        private SetFactory( Random random, RandomBeanGenerator rnd ) {
            super(
                random,
                rnd,
                type -> type.isSet() && type.getClassGenerics().size() == 1,
                () -> new HashSet<>(),
                type -> type.getClassGenerics().get( 0 ),
                Set::add
            );
        }
    }

    private static abstract class CollectionFactory<C,E> implements Factory<C> {
        private final Random                        random;
        private final RandomBeanGenerator           factory;
        private final Predicate<JavaClass>          isSupportedPredicate;
        private final Supplier<C>                   collectionCreator;
        private final Function<JavaClass,JavaClass> getComponentTypeFrom;
        private final BiConsumer<C,E>               addIntoCollection;

        private CollectionFactory(
            Random                        random,
            RandomBeanGenerator           factory,
            Predicate<JavaClass>          isSupportedPredicate,
            Supplier<C>                   collectionCreator,
            Function<JavaClass,JavaClass> getComponentTypeFrom,
            BiConsumer<C, E>              addIntoCollection
        ) {
            this.random               = random;
            this.factory              = factory;
            this.isSupportedPredicate = isSupportedPredicate;
            this.collectionCreator    = collectionCreator;
            this.getComponentTypeFrom = getComponentTypeFrom;
            this.addIntoCollection    = addIntoCollection;
        }

        public boolean supports( JavaClass type ) {
            return isSupportedPredicate.test(type);
        }

        public C create( JavaClass type, String name ) {
            int         numElements      = random.nextInt( 0, 12 );
            C           collection       = collectionCreator.get();
            JavaClass   componentType    = getComponentTypeFrom.apply( type );
            Supplier<E> componentFactory = factory.createSupplierFor( componentType, name );

            for ( int i=0; i<numElements; i++ ) {
                E newElement = componentFactory.get();

                addIntoCollection.accept( collection, newElement );
            }

            return collection;
        }
    }

    private static class MapFactory implements Factory<Object> {
        private final Random              random;
        private final RandomBeanGenerator factory;

        private MapFactory( Random random, RandomBeanGenerator factory ) {
            this.random  = random;
            this.factory = factory;
        }

        public boolean supports( JavaClass type ) {
            return type.isMap() && type.getClassGenerics().size() == 2;
        }

        public Object create( JavaClass type, String name ) {
            List<JavaClass> typeParameters = type.getClassGenerics();
            JavaClass       keyType        = typeParameters.get( 0 );
            JavaClass       valueType      = typeParameters.get( 1 );

            Map<Object,Object> newMap = new HashMap<>();
            int numElements = random.nextInt( 0, 12 );

            for ( int i=0; i<numElements; i++ ) {
                Object key   = factory.rnd( keyType, name );
                Object value = factory.rnd( valueType, name );

                newMap.put( key, value );
            }

            return newMap;
        }
    }

    private static class StringFactory implements Factory<String> {
        private final Random random;

        private StringFactory( Random random ) {
            this.random = random;
        }

        public boolean supports( JavaClass type ) {
            return ReflectionUtils.isCastableTo(type.getJdkClass(), String.class);
        }

        public String create( JavaClass type, String name ) {
            return random.nextText();
        }
    }

    private static class StaticMethodFactory implements Factory<Object> {
        private final RandomBeanGenerator rnd;

        private StaticMethodFactory( RandomBeanGenerator rnd ) {
            this.rnd = rnd;
        }

        public boolean supports( JavaClass type ) {
            return selectFactoryMethodFor(type).isPresent();
        }

        public Object create( JavaClass type, String name ) {
            JavaMethod factoryMethod = selectFactoryMethodFor( type ).get();
            Object[]   parameters    = rnd.createRndArgsForExecutable(factoryMethod);

            return factoryMethod.invoke( null, parameters );
        }

        private static Optional<JavaMethod> selectFactoryMethodFor( JavaClass type ) {
            Optional<JavaMethod> specialFactory = type.getMethods().stream()
                .filter( JavaExecutable::isStatic )
                .filter( m -> m.getName().equals("rnd") )
                .filter( m -> m.getParameterCount() == 1 )
                .findFirst();

            if ( specialFactory.isPresent() ) {
                return specialFactory;
            }

            return type.getMethods().stream()
                .filter( JavaExecutable::isStatic )
                .filter( m -> m.getName().startsWith("create"))
                .filter( m -> m.getParameterCount() == 1 )
                .findFirst();
        }
    }

    private <T> Object[] createRndArgsForExecutable( JavaExecutable executable ) {
        return executable.getParameters().stream()
            .map( p -> rnd(p) )
            .toArray();
    }

    private static class DefaultBeanFactory implements Factory<Object> {
        private RandomBeanGenerator rnd;

        private DefaultBeanFactory( RandomBeanGenerator rnd ) {
            this.rnd = rnd;
        }

        public boolean supports( JavaClass type ) {
            return optionalCreate( type, null ).isPresent();
        }

        public Object create( JavaClass type, String name ) {
            return optionalCreate(type,name).get();
        }

        public Optional optionalCreate( JavaClass type, String name ) {
            Optional<JavaConstructor> constructorOptional = selectConstructorFor( type );

            return constructorOptional.map( constructor -> {
                Object[] args = rnd.createRndArgsForExecutable( constructor );

                return constructor.newInstance(args);
            });
        }

        @SuppressWarnings("rawtypes")
        private <T> Optional<JavaConstructor> selectConstructorFor( JavaClass type ) {
            Comparator<JavaConstructor> parameterLengthComparator = Comparator.comparingInt( JavaConstructor::getParameterCount );

            return type.getConstructors().stream()
                .filter( JavaExecutable::isPublic )
                .max( parameterLengthComparator );
        }
    }
}
