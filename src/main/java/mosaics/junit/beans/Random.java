package mosaics.junit.beans;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import lombok.Getter;

import java.util.function.Supplier;


public class Random {
    @Getter private final long                        seed;
            private final java.util.Random            rnd;
            private final RandomJapeneseTextGenerator japaneseGenerator;
            private final Lorem                       loremGenerator;

    public Random( long seed ) {
        this.seed              = seed;
        this.rnd               = new java.util.Random(seed);
        this.japaneseGenerator = new RandomJapeneseTextGenerator(this);
        this.loremGenerator    = new LoremIpsum( seed );
    }

    public boolean nextBoolean() {
        return rnd.nextBoolean();
    }

    public byte nextByte() {
        return (byte) (rnd.nextInt(Byte.MAX_VALUE+1) * salt());
    }

    public short nextShort() {
        return (short) (rnd.nextInt(Short.MAX_VALUE+1) * salt());
    }

    public int nextInt() {
        return rnd.nextInt(Integer.MAX_VALUE) * salt();
    }

    public int nextInt( int min, int maxExc ) {
        return min + rnd.nextInt( maxExc-min );
    }

    public long nextLong() {
        return rnd.nextLong() * salt();
    }

    public float nextFloat() {
        return rnd.nextFloat() * Float.MAX_VALUE * salt();
    }

    public double nextDouble() {
        return rnd.nextDouble() * Double.MAX_VALUE * salt();
    }

    public char nextChar() {
        return (char) nextInt(32,127);
    }

    public String nextText() {
        return eitherOr(100, this::nextJapaneseSentence, this::nextLoremSentence );
    }

    /**
     * Randomly select between returning a value from one of the two suppliers.
     * If oneInChance is 10, then there is a 1 in 10 chance of supplier a being chosen
     * and a 9 in 10 chance of b.
     */
    public <T> T eitherOr( int oneInChance, Supplier<T> a, Supplier<T> b ) {
        return isOneIn(oneInChance) ? a.get() : b.get();
    }

    public <T> T eitherOr( Supplier<T> a, Supplier<T> b ) {
        return isOneIn(2) ? a.get() : b.get();
    }

    public String nextLoremSentence() {
        return nextLoremSentence( 0, 6 );
    }

    public String nextLoremSentence( int minWords, int maxWords ) {
        return loremGenerator.getWords( minWords, maxWords );
    }

    public String nextJapaneseSentence() {
        return nextJapaneseSentence( 0, 10 );
    }

    public String nextJapaneseSentence( int minCharacters, int maxCharacters ) {
        return japaneseGenerator.generateText( nextInt(minCharacters,maxCharacters) );
    }

    public String nextCity() {
        return loremGenerator.getCity();
    }

    public String nextCountry() {
        return loremGenerator.getCountry();
    }

    public String nextEmail() {
        return loremGenerator.getEmail();
    }

    public String nextFirstName() {
        return loremGenerator.getFirstName();
    }

    public String nextFirstNameMale() {
        return loremGenerator.getFirstNameMale();
    }

    public String nextFirstNameFemale() {
        return loremGenerator.getFirstNameFemale();
    }

    public String nextLastName() {
        return loremGenerator.getLastName();
    }

    public String nextName() {
        return loremGenerator.getName();
    }

    public String nextNameMale() {
        return loremGenerator.getNameMale();
    }

    public String nextNameFemale() {
        return loremGenerator.getNameFemale();
    }

    public String nextTitle(int count) {
        return loremGenerator.getTitle( count );
    }

    public String nextTitle(int min, int max) {
        return loremGenerator.getTitle( min, max );
    }

    public String nextPhone() {
        return loremGenerator.getPhone();
    }

    public String nextStateAbbr() {
        return loremGenerator.getStateAbbr();
    }

    public String nextStateFull() {
        return loremGenerator.getStateFull();
    }

    public String nextZipCode() {
        return loremGenerator.getZipCode();
    }

    private int salt() {
        return isOneIn(20) ? -1 : 1;
    }

    public boolean isOneIn( int i ) {
        return nextInt(0, i) == 0;
    }
}
