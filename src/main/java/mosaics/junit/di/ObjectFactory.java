package mosaics.junit.di;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import mosaics.junit.lang.ReflectionUtils;
import mosaics.junit.lang.reflection.JavaClass;
import mosaics.junit.lang.reflection.JavaParameter;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;


/**
 * A factory for creating objects that are to be injected into a test's field or method parameter.
 */
public interface ObjectFactory {

    public static ObjectFactory newFactory( Class<?> type, BiFunction<ObjectFactory,JavaParameter,Object> factory ) {
        return newFactory(
            ObjectFactoryUtils.createDefaultSupportsFunctionFor(type),
            factory,
            ObjectFactoryUtils::createDefaultDestructorFor
        );
    }

    public static ObjectFactory newFactory( Class<?> type, BiFunction<ObjectFactory,JavaParameter,Object> factory, Consumer<Object> destructor ) {
        return newFactory(
            ObjectFactoryUtils.createDefaultSupportsFunctionFor(type),
            factory,
            destructor
        );
    }

    public static ObjectFactory newFactory( Predicate<JavaParameter> supportsF, BiFunction<ObjectFactory,JavaParameter,Object> factory ) {
        return newFactory( supportsF, factory, ObjectFactoryUtils::createDefaultDestructorFor );
    }

    public static ObjectFactory newFactory(
        Predicate<JavaParameter> supportsF,
        BiFunction<ObjectFactory,JavaParameter,Object> factory,
        Consumer<Object> destructor
    ) {
        return new DefaultObjectFactory(supportsF, factory, destructor);
    }

    public static ObjectFactory or( ObjectFactory...factories ) {
        ObjectFactory factory = factories[0];

        for ( int i=1; i<factories.length; i++ ) {
            factory = factory.or( factories[i] );
        }

        return factory;
    }


    /**
     * Returns true if this factory knows how to create instances for the specified variable.
     */
    public boolean supports( JavaParameter javaParameter );

    /**
     * Create an instance for the specified variable.  It may or may not create a new instance, as
     * it may choose to reuse pre-existing instances.
     */
    public Object create( JavaParameter javaParameter );

    /**
     * Get an instance that will fit into the specified variable if and only if it already has
     * an allocated instance handy.  If it does not have a pre-existing instance, then it will return
     * EMPTY.
     */
    public default Object create( Class t ) {
        return create( JavaParameter.of(t) );
    }

    public default Object create( JavaClass t ) {
        return create( JavaParameter.of(t) );
    }

    /**
     * Get an instance that will fit into the specified variable if and only if it already has
     * an allocated instance handy.  If it does not have a pre-existing instance, then it will return
     * EMPTY.
     */
    public Optional<Object> get( JavaParameter javaParameter );

    /**
     * Get an instance that will fit into the specified variable if and only if it already has
     * an allocated instance handy.  If it does not have a pre-existing instance, then it will return
     * EMPTY.
     */
    public default Optional<Object> get( Class t ) {
        return get( JavaParameter.of(t) );
    }

    /**
     * Release all objects that this cache has previously allocated.
     */
    public void releaseAll();


    /**
     * Create an object that can be injected into the specified field or parameter.
     *
     * @return None if this factory cannot create an object for that variable or Some containing
     *    an object that may be used for that variable.
     */
    public default Optional<Object> tryToCreate( JavaParameter v ) {
        if ( !supports(v) ) {
            return Optional.empty();
        }

        return Optional.ofNullable( create(v) );
    }


    /**
     * Creates a composite ObjectFactory out of the two specified objectFactories.  Each factory
     * starting with `this` factory is asked to create an object for the specified variable.  The
     * first one to return a none empty optional, ends the search.
     */
    public default ObjectFactory or( ObjectFactory otherObjectFactory ) {
        return new OrObjectFactory( this, otherObjectFactory );
    }

    public default void throwIfNotSupported( JavaParameter javaParameter ) {
        if ( !supports(javaParameter) ) {
            throw new IllegalArgumentException( javaParameter + " is not supported by " + this );
        }
    }

    public default boolean supports( Field f) {
        return supports(JavaParameter.of(f));
    }
}

class DefaultObjectFactory implements ObjectFactory {
    private final Predicate<JavaParameter>                       supportsF;
    private final BiFunction<ObjectFactory,JavaParameter,Object> factoryF;
    private final Consumer<Object>                                 destructorF;

    private final Map<CacheKey,Object>              cache       = new ConcurrentHashMap<>();

    public DefaultObjectFactory( Predicate<JavaParameter> supportsF, BiFunction<ObjectFactory, JavaParameter, Object> factoryF, Consumer<Object> destructorF ) {
        this.supportsF   = supportsF;
        this.factoryF    = factoryF;
        this.destructorF = destructorF;
    }

    public boolean supports( JavaParameter javaParameter ) {
        return supportsF.test( javaParameter );
    }

    public synchronized Object create( JavaParameter javaParameter ) {
        CacheKey cacheKey = new CacheKey( javaParameter );

        return cache.computeIfAbsent(cacheKey, key -> factoryF.apply(this, javaParameter) );
    }

    public Optional<Object> get( JavaParameter javaParameter ) {
        return get( javaParameter.type().getJdkClass(), javaParameter.name() );
    }

    public synchronized Optional<Object> get( Class type, String name ) {
        return Optional.ofNullable( cache.get(new CacheKey(type,name)) );
    }

    public synchronized void releaseAll() {
        cache.entrySet().forEach( kv -> destructorF.accept(kv.getValue()) );
        cache.clear();
    }

    private static record CacheKey(Class type, String name) {
        public CacheKey( JavaParameter javaParameter ) {
            this( javaParameter.type().getJdkClass(), javaParameter.name() );
        }
    }
}

class ObjectFactoryUtils {
    public static Predicate<JavaParameter> createDefaultSupportsFunctionFor( Class<?> type ) {
        return javaParameter -> ReflectionUtils.isCastableTo(type,javaParameter.type().getJdkClass());
    }

    @SneakyThrows
    public static void createDefaultDestructorFor( Object o ) {
        if ( o instanceof AutoCloseable ) {
            ((AutoCloseable) o).close();
        }
    }
}

@AllArgsConstructor
@EqualsAndHashCode
class OrObjectFactory implements ObjectFactory {
    private final ObjectFactory a;
    private final ObjectFactory b;

    public String toString() {
        return "OrElse("+a+", "+b+")";
    }

    public boolean supports( JavaParameter javaParameter ) {
        return a.supports( javaParameter ) || b.supports( javaParameter );
    }

    public Object create( JavaParameter javaParameter ) {
        return a.supports( javaParameter ) ? a.create( javaParameter ) : b.create( javaParameter );
    }

    public Optional<Object> get( JavaParameter javaParameter ) {
        return a.get(javaParameter).or( () -> b.get(javaParameter) );
    }

    public void releaseAll() {
        a.releaseAll();
        b.releaseAll();
    }
}
