package mosaics.junit.beans;

//import com.softwaremosaic.lang.reflection.Capture;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class RandomBeanGeneratorTest {

    private RandomBeanGenerator factory = new RandomBeanGenerator();

    @Test
    public void rndEnum() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(RGB.class));
    }

    @Test
    public void rndEnumDto() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(RGBDto.class));
    }

    @Test
    public void rndBooleanPrimitives() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(Boolean.TYPE));
    }

    @Test
    public void rndBean_booleanPrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(BooleanPrimitiveDto.class));
    }

    @Test
    public void rndBean_booleanObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(BooleanObjectDto.class));
    }

    @Test
    public void rndBean_bytePrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(BytePrimitiveDto.class));
    }

    @Test
    public void rndBean_byteObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ByteObjectDto.class));
    }

    @Test
    public void rndBean_characterPrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(CharacterPrimitiveDto.class));
    }

    @Test
    public void rndBean_characterObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(CharacterObjectDto.class));
    }

    @Test
    public void rndBean_shortPrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ShortPrimitiveDto.class));
    }

    @Test
    public void rndBean_shortObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ShortObjectDto.class));
    }

    @Test
    public void rndBean_integerPrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(IntegerPrimitiveDto.class));
    }

    @Test
    public void rndBean_integerObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(IntegerObjectDto.class));
    }

    @Test
    public void rndBean_longPrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(LongPrimitiveDto.class));
    }

    @Test
    public void rndBean_longObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(LongObjectDto.class));
    }

    @Test
    public void rndBean_floatPrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(FloatPrimitiveDto.class));
    }

    @Test
    public void rndBean_floatObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(FloatObjectDto.class));
    }

    @Test
    public void rndBean_doublePrimitive() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(DoublePrimitiveDto.class));
    }

    @Test
    public void rndBean_doubleObject() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(DoubleObjectDto.class));
    }

    @Test
    public void rndBean_arrayOfBooleans() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ArrayOfBooleansDto.class));
    }

    @Test
    public void rndBean_arrayOfInts() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ArrayOfIntsDto.class));
    }

    @Test
    public void rndBean_arrayOfStringDto() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ArrayOfStringDto.class));
    }

    @Test
    public void rndBean_optionalStringDto() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(OptionalStringDto.class));
    }

    @Test
    public void rndBean_listOfStringsDto() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(ListOfStringsDto.class));
    }

    @Test
    public void rndBean_setOfStringsDto() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(SetOfStringsDto.class));
    }

    @Test
    public void rndBean_mapOfStringsDto() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(MapOfStringsDto.class));
    }

    @Test
    public void rndBean_containerDtoWithItsOwnStaticFactoryMethod() {
        assertfactoryGeneratesRandomValues(() -> factory.rnd(OptionOfDtoWithFactoryMethod.class));
    }

    @Test
    public void rndBean_string() {
        StringDto dto = factory.rnd( StringDto.class );

        assertTrue( dto.name != null );
    }

    @Test
    public void rndBean_stringWithNoErasureAnnotation() {
        StringWithNoErasureAnnotationDto dto = factory.rnd( StringWithNoErasureAnnotationDto.class );

        assertTrue( dto.name != null );
    }

    @Test
    public void rndBean_StringDtoWithGenericFactoryMethod() {
        StringDtoWithGenericFactoryMethod dto = factory.rnd( StringDtoWithGenericFactoryMethod.class );

        assertTrue( dto.ref != null );
        assertTrue( dto.ref.field != null );
    }

    @Test
    public void rndBean_UsingStaticMethodFactoryThatItSelfTakesAFactory() {
        DtoWithStaticFactoryMethodThatItSelfTakesAFactory dto = factory.rnd( DtoWithStaticFactoryMethodThatItSelfTakesAFactory.class );

        assertTrue( dto.field != null );
        assertTrue( dto.field.field != null );
    }


    private void assertfactoryGeneratesRandomValues( Supplier factory ) {
        Object[] rndValues = Stream.generate(factory).limit(20).toArray();

        assertNotAllIdentical( rndValues );
    }

    private void assertNotAllIdentical( Object[] rndValues ) {
        assertTrue( Arrays.stream( rndValues ).distinct().count() > 1, Arrays.asList(rndValues) + " are all identical" );
    }


    public static enum RGB {
        RED, GREEN, BLUE;
    }


    @Value
    public static class RGBDto {
        private RGB value;
    }

    @Value
    public static class BooleanPrimitiveDto {
        private boolean flag;
    }

    @Value
    public static class BooleanObjectDto {
        private Boolean flag;
    }

    @Value
    public static class BytePrimitiveDto {
        private byte value;
    }

    @Value
    public static class ByteObjectDto {
        private Byte value;
    }

    @Value
    public static class CharacterPrimitiveDto {
        private char value;
    }

    @Value
    public static class CharacterObjectDto {
        private Character value;
    }

    @Value
    public static class ShortPrimitiveDto {
        private short value;
    }

    @Value
    public static class ShortObjectDto {
        private Short value;
    }

    @Value
    public static class IntegerPrimitiveDto {
        private int value;
    }

    @Value
    public static class IntegerObjectDto {
        private Integer value;
    }

    @Value
    public static class LongPrimitiveDto {
        private long value;
    }

    @Value
    public static class LongObjectDto {
        private Long value;
    }

    @Value
    public static class FloatPrimitiveDto {
        private float value;
    }

    @Value
    public static class FloatObjectDto {
        private Float value;
    }

    @Value
    public static class DoublePrimitiveDto {
        private double value;
    }

    @Value
    public static class DoubleObjectDto {
        private Double value;
    }

    @Value
    public static class StringDto {
        private String name;
    }

    @Value
    public static class StringWithNoErasureAnnotationDto {
        private String name;
    }

    @Value
    public static class ArrayOfBooleansDto {
        private boolean[] value;
    }

    @Value
    public static class ArrayOfIntsDto {
        private int[] value;
    }

    @Value
    public static class ArrayOfStringDto {
        private StringDto[] value;
    }

    @Value
    public static class OptionalStringDto {
        private Optional<String> value;
    }

    @Value
    public static class ListOfStringsDto {
        private List<String> value;
    }

    @Value
    public static class SetOfStringsDto {
        private Set<String> value;
    }

    @Value
    public static class MapOfStringsDto {
        private Map<String,Integer> value;
    }

    @SuppressWarnings("unused")
    @ToString
    @EqualsAndHashCode
    public static class DtoWithFactoryMethod {
        public static DtoWithFactoryMethod createInstance( String v ) {
            DtoWithFactoryMethod dto = new DtoWithFactoryMethod();
            dto.field = v;

            return dto;
        }

        private String field;

        private DtoWithFactoryMethod() {}
    }

    @Value
    public static class OptionOfDtoWithFactoryMethod {
        private Optional<DtoWithFactoryMethod> value;
    }


    @SuppressWarnings({"unused", "rawtypes"})
    @ToString
    @EqualsAndHashCode
    public static class DtoWithGenericFactoryMethod<T> {
        public static <T> DtoWithGenericFactoryMethod<T> createInstance( T v ) {
            DtoWithGenericFactoryMethod o = new DtoWithGenericFactoryMethod();
            o.field = v;

            return o;
        }

        private T field;

        private DtoWithGenericFactoryMethod() {}
    }

    @SuppressWarnings({"unused", "rawtypes"})
    @ToString
    @EqualsAndHashCode
    public static class DtoWithStaticFactoryMethodThatItSelfTakesAFactory {
        public static DtoWithStaticFactoryMethodThatItSelfTakesAFactory rnd( RandomFactory factory ) {
            DtoWithStaticFactoryMethodThatItSelfTakesAFactory o = new DtoWithStaticFactoryMethodThatItSelfTakesAFactory();
            o.field = factory.rnd( DtoWithFactoryMethod.class );

            return o;
        }

        private DtoWithFactoryMethod field;

        private DtoWithStaticFactoryMethodThatItSelfTakesAFactory() {}
    }


    @Value
    public static class StringDtoWithGenericFactoryMethod {
        private DtoWithGenericFactoryMethod<String> ref;
    }

}
