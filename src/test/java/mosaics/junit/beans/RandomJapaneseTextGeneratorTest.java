package mosaics.junit.beans;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class RandomJapaneseTextGeneratorTest {
    private RandomJapeneseTextGenerator rnd = new RandomJapeneseTextGenerator();

    @Test
    public void generateJapaneseText() {
        for ( int i=0; i<1000; i++ ) {
            String text = rnd.generateText( 10 );

            assertTrue( text.length() == 10);

            for ( char c : text.toCharArray() ) {
                assertTrue( c > 255 || Character.isDigit(c), () -> "char '"+c+"'" );
            }
        }
    }

}
