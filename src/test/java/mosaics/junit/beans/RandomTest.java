package mosaics.junit.beans;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RandomTest {
    private static final int SAMPLE_COUNT = 10_000;

    private final Random rnd = new Random( System.currentTimeMillis() );

    @Test
    public void testNextBoolean() {
        Map<Boolean,Integer> samples = sampleAndCountResults( rnd::nextBoolean );

        assertEquals( 2, samples.size() );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
    }

    @Test
    public void testNextByte() {
        Map<Byte,Integer> samples = sampleAndCountResults( rnd::nextByte );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
    }

    @Test
    public void testNextShort() {
        Map<Short,Integer> samples = sampleAndCountResults( rnd::nextShort );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
        assertTrue( hasPostiveValues(samples) );
        assertTrue( hasNegativeValues(samples) );
    }

    @Test
    public void testNextInt() {
        Map<Integer,Integer> samples = sampleAndCountResults( rnd::nextInt );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
        assertTrue( hasPostiveValues(samples) );
        assertTrue( hasNegativeValues(samples) );
    }

    @Test
    public void testNextIntRange() {
        for ( int i=0; i<SAMPLE_COUNT; i++ ) {
            int v = rnd.nextInt( 10, 1000 );

            assertTrue( v >= 10 );
            assertTrue( v < 1000 );
        }

        for ( int i=0; i<SAMPLE_COUNT; i++ ) {
            int v = rnd.nextInt( -1000, -10 );

            assertTrue( v >= -1000 );
            assertTrue( v < -10 );
        }

        for ( int i=0; i<SAMPLE_COUNT; i++ ) {
            int v = rnd.nextInt( -10, 10 );

            assertTrue( v >= -10 );
            assertTrue( v < 10 );
        }
    }

    @Test
    public void testNextLong() {
        Map<Long,Integer> samples = sampleAndCountResults( rnd::nextLong );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
        assertTrue( hasPostiveValues(samples) );
        assertTrue( hasNegativeValues(samples) );
    }

    @Test
    public void testNextFloat() {
        Map<Float,Integer> samples = sampleAndCountResults( rnd::nextFloat );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
        assertTrue( hasPostiveValues(samples) );
        assertTrue( hasNegativeValues(samples) );
    }

    @Test
    public void testNextDouble() {
        Map<Double,Integer> samples = sampleAndCountResults( rnd::nextDouble );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
        assertTrue( hasPostiveValues(samples) );
        assertTrue( hasNegativeValues(samples) );
    }

    @Test
    public void testNextChar() {
        Map<Character,Integer> samples = sampleAndCountResults( rnd::nextChar );

        assertTrue( samples.size() > 10 );
        assertEquals( SAMPLE_COUNT, sumCounts(samples) );
        assertTrue( samples.values().stream().anyMatch( c -> c > 'z' ));
        assertTrue( samples.values().stream().anyMatch( c -> c < 'a' ));
    }


    private <T> Map<T,Integer> sampleAndCountResults( Supplier<T> f ) {
        Map<T,Integer> counts = new HashMap<>();

        for ( int i=0; i<SAMPLE_COUNT; i++ ) {
            T v = f.get();

            counts.compute( v, (v2,countSoFar) -> Objects.requireNonNullElse(countSoFar,0) + 1 );
        }

        return counts;
    }

    private <T> int sumCounts( Map<T, Integer> counts ) {
        return counts.values().stream().mapToInt( Integer::intValue ).sum();
    }

    private <T extends Number> boolean hasNegativeValues( Map<T, Integer> counts ) {
        return counts.keySet().stream().anyMatch( v -> v.doubleValue() < 0 );
    }

    private <T extends Number> boolean hasPostiveValues( Map<T, Integer> counts ) {
        return counts.keySet().stream().anyMatch( v -> v.doubleValue() >= 0 );
    }
}
