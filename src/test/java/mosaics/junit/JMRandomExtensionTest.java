package mosaics.junit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import mosaics.junit.JMRandomExtension.Random;
import mosaics.junit.beans.RandomBeanGenerator;
import mosaics.junit.lang.reflection.JavaField;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;


/**
 * Supplies randomly generated values to fields on test classes and parameters on test methods.
 */
@ExtendWith(JMRandomExtension.class)
public class JMRandomExtensionTest extends JMAssertions {
    private @Random int field1;
    private @Random(generator=GeneratorWithStringArg.class) String field2;

    @Test
    public void testRandomString( @Random String rndName ) {
        assertNotNull( rndName );
    }

    public static class GeneratorWithNoArgConstructor implements Supplier<String> {
        public String get() {
            return "123456789";
        }
    }

    @Test
    public void testGeneratorWithNoArgConstructor(
        @Random(generator=GeneratorWithNoArgConstructor.class) String rndName
    ) {
        assertEquals( "123456789", rndName );
    }

    @AllArgsConstructor
    public static class GeneratorWithRandomBeanGeneratorArg implements Supplier<String> {
        private RandomBeanGenerator generator;

        public String get() {
            return generator == null ? "null" : "has generator";
        }
    }

    @AllArgsConstructor
    public static class GeneratorWithStringArg implements Supplier<String> {
        private String name;

        public String get() {
            return name;
        }
    }

    @Test
    public void testGeneratorWithStringArg_expectParamName(
        @Random(generator=GeneratorWithStringArg.class) String rndName
    ) {
        assertEquals( "rndName", rndName );
    }

    @Test
    public void testGeneratorWithStringArgOnField_expectParamName() {
        assertEquals( "field2", field2 );
    }


    public static class DTO {
        @Getter private final String description;

        public DTO( @Random(generator=GeneratorWithStringArg.class) String description ) {
            this.description = description;
        }
    }

    @Test
    public void testGeneratorWithStringArgWithinDTO_expectParamName(
        @Random() DTO dto
    ) {
        assertEquals( "description", dto.getDescription() );
    }

    @AllArgsConstructor
    public static class GeneratorWithRandomArg implements Supplier<String> {
        private final mosaics.junit.beans.Random rnd;

        public String get() {
            return rnd == null ? "null" : "has rnd";
        }
    }

    @Test
    public void testGeneratorWithRandomArg(
        @Random(generator=GeneratorWithRandomArg.class) String rndName
    ) {
        assertEquals( "has rnd", rndName );
    }

    @Test
    public void testRandomField() {
        assertIsNotZero( field1 );
    }

    @Test
    public void testRandomStringDto( @Random StringBean rndBean ) {
        assertNotNull( rndBean );
        assertNotNull( rndBean.getName() );
    }

    @Test
    public void showThatRandomTestMethodParametersUseARandomSeed() {
        noSeedTest( CaptureTestMethodParameterWithOutSeed.class );
    }

    @Test
    public void showThatRandomTestMethodParametersUseCanUseASpecifiedSeed() {
        withSeedTest( CaptureTestMethodParameterWithSeed.class );
    }

    @Test
    public void showThatRandomFieldUsesARandomSeed() {
        noSeedTest( CaptureFieldWithOutSeed.class );
    }

    @Test
    public void showThatRandomFieldCanUseASpecifiedSeed() {
        withSeedTest( CaptureFieldWithSeed.class );
    }

    @Test
    public void showThatRandomStaticFieldUsesARandomSeed() {
        noSeedTest( CaptureStaticFieldWithOutSeed.class );
    }

    @Test
    public void showThatRandomStaticFieldCanUseASpecifiedSeed() {
        withSeedTest( CaptureStaticFieldWithSeed.class );
    }



    private static void noSeedTest( Class testClass ) {
        JUnitHarness.runUnitTests( testClass );
        StringBean firstBean = (StringBean) JavaField.of(testClass,"generatedBean").get().getValue(AtomicReference.class).get();
        JavaField.of(testClass,"field").ifPresent( f -> {if (f.isStatic()) f.clear();} );

        JUnitHarness.runUnitTests( testClass );
        StringBean secondBean = (StringBean) JavaField.of(testClass,"generatedBean").get().getValue(AtomicReference.class).get();;
        JavaField.of(testClass,"field").ifPresent( f -> {if (f.isStatic()) f.clear();} );

//        if ( isBlank(firstBean.name) && isBlank(secondBean.name) ) {
//            return;
//        }

        assertNotEquals( firstBean, secondBean );
    }

    private static void withSeedTest( Class testClass ) {
        JUnitHarness.runUnitTests( testClass );
        StringBean firstBean = (StringBean) JavaField.of(testClass,"generatedBean").get().getValue(AtomicReference.class).get();
        JavaField.of(testClass,"field").ifPresent( f -> {if (f.isStatic()) f.clear();} );


        JUnitHarness.runUnitTests( testClass );
        StringBean secondBean = (StringBean) JavaField.of(testClass,"generatedBean").get().getValue(AtomicReference.class).get();;
        JavaField.of(testClass,"field").ifPresent( f -> {if (f.isStatic()) f.clear();} );

        assertEquals( firstBean, secondBean );
    }

    @Value
    public static class StringBean {
        private String name;
    }

    @Tag("internal")
    @ExtendWith(JMRandomExtension.class)
    public static class CaptureTestMethodParameterWithOutSeed {
        private static final AtomicReference<StringBean> generatedBean = new AtomicReference<>();

        @Test
        public void generateBean( @Random StringBean bean ) {
            generatedBean.set( bean );
        }
    }

    @Tag("internal")
    @JMRandomExtension.RandomSeed(1234L)
    @ExtendWith(JMRandomExtension.class)
    public static class CaptureTestMethodParameterWithSeed {
        private static final AtomicReference<StringBean> generatedBean = new AtomicReference<>();

        @Test
        public void generateBean( @Random StringBean bean ) {
            generatedBean.set( bean );
        }
    }

    @Tag("internal")
    @ExtendWith(JMRandomExtension.class)
    public static class CaptureFieldWithOutSeed {
        private static final AtomicReference<StringBean> generatedBean = new AtomicReference<>();

        private @Random StringBean field;

        @Test
        public void generateBean() {
            generatedBean.set( field );
        }
    }

    @Tag("internal")
    @JMRandomExtension.RandomSeed(1234L)
    @ExtendWith(JMRandomExtension.class)
    public static class CaptureFieldWithSeed {
        private static final AtomicReference<StringBean> generatedBean = new AtomicReference<>();

        private @Random StringBean field;

        @Test
        public void generateBean() {
            generatedBean.set( field );
        }
    }

    @Tag("internal")
    @ExtendWith(JMRandomExtension.class)
    public static class CaptureStaticFieldWithOutSeed {
        private static final AtomicReference<StringBean> generatedBean = new AtomicReference<>();

        private static @Random StringBean field;

        @Test
        public void generateBean() {
            generatedBean.set( field );
        }
    }

    @Tag("internal")
    @JMRandomExtension.RandomSeed(1234L)
    @ExtendWith(JMRandomExtension.class)
    public static class CaptureStaticFieldWithSeed {
        private static final AtomicReference<StringBean> generatedBean = new AtomicReference<>();

        private static @Random StringBean field;

        @Test
        public void generateBean() {
            generatedBean.set( field );
        }
    }

}
