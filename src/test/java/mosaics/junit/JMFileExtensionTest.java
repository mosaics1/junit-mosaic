package mosaics.junit;

import mosaics.junit.JMFileExtension.DirectoryContents;
import mosaics.junit.JMFileExtension.FileContents;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings({"unused", "WeakerAccess"})
@ExtendWith( JMFileExtension.class )
public class JMFileExtensionTest {

    private static Set<File> filesAwaitingDeletion = Collections.synchronizedSet( new HashSet<>() );

    private File dir1;
    private File dir2;

    @FileContents(lines={"abc","def"})
    private File fileWithContents;

    @DirectoryContents({
        @FileContents(name="a.txt", lines={"abc"}),
        @FileContents(name="foo/bar.txt", lines={"123"}),
        @FileContents(name="/a/b/c.txt", lines={"123456"})
    })
    private File directoryWithContents;


    @DirectoryContents({
        @FileContents(name="a.txt", lines={"abc"}),
        @FileContents(name="foo/bar.txt", lines={"123"}),
        @FileContents(name="/a/b/c.txt", lines={"123456"})
    })
    private static File staticDirectoryWithContents;


    @DirectoryContents({
        @FileContents(name="m1.txt", lines={"abc","123"}),
        @FileContents(name="m2.txt", lines={
                """
                |abc def
                |123 456
                """
        })
    })
    private static File directoryContainingmultilineFiles;


    private static boolean tmpSet;
    private static boolean tmp2Set;
    private static Object tmp;
    private static Object tmp2;


    @AfterAll
    public static void tearDown() {
        for ( File f : filesAwaitingDeletion ) {
            assertFalse( f.exists(), f.getAbsolutePath() );
        }
    }

    // GENERATE DIRECTORY
    @Nested
    public class EmptyDirectoryTestCases {
        @Test
        public void givenMethodParameter_injectTmpDirectory( File tmp ) {
            assertNotNull( tmp );
            assertTrue( tmp.isDirectory() );
            assertTrue( tmp.exists() );

            filesAwaitingDeletion.add( tmp );
        }

        @Test
        public void givenMethodParameter_injectTwoTmpDirectories( File tmp1, File tmp2 ) {
            assertTwoDirectories( tmp1, tmp2 );
        }

        @Test
        public void givenInstanceVariable_injectTmpDirectory() {
            assertTwoDirectories( dir1, dir2 );

            filesAwaitingDeletion.clear();
        }
    }


    @Test
    public void multilineFileTests() throws FileNotFoundException {
        assertTrue( directoryContainingmultilineFiles.exists(), directoryContainingmultilineFiles.getName() );

        assertFileContentsEquals( "abc\n123", new File(directoryContainingmultilineFiles, "m1.txt") );
        assertFileContentsEquals( "abc def\n123 456", new File(directoryContainingmultilineFiles, "m2.txt") );

        filesAwaitingDeletion.add( directoryContainingmultilineFiles );
        filesAwaitingDeletion.clear();
    }


    @Test
    public void givenInstanceVariableDirectoryWithContents() throws FileNotFoundException {
        assertTrue( directoryWithContents.exists() );

        assertFileContentsEquals( "abc", new File(directoryWithContents, "a.txt") );
        assertFileContentsEquals( "123", new File(directoryWithContents, "foo/bar.txt") );
        assertFileContentsEquals( "123456", new File(directoryWithContents, "a/b/c.txt") );

        filesAwaitingDeletion.add( directoryWithContents );

        filesAwaitingDeletion.clear();
    }

    @Test
    public void givenInstanceParameterDirectoryWithContents(
        @DirectoryContents({
            @FileContents(name="/a.txt", lines={"12345"}),
            @FileContents(name="/b/b.txt", lines={"112233"})
        })
        File directory
    ) throws FileNotFoundException {
        assertTrue( directory.exists() );

        assertFileContentsEquals( "12345", new File(directory, "a.txt") );
        assertFileContentsEquals( "112233", new File(directory, "b/b.txt") );

        filesAwaitingDeletion.add( directory );
    }

    @Test
    public void givenLastModified(
        @FileContents(name="/a.txt", lines={"12345"}, lastModified = "2020-05-10T10:44:11Z") File f
    ) {
        String actual = DateTimeFormatter.ISO_DATE_TIME.format( new Date( f.lastModified() ).toInstant().atZone( ZoneId.of( "UTC" ) )
            .toLocalDateTime() );

        assertEquals("2020-05-10T10:44:11", actual );
    }

    @Test
    public void loadARelativeResource(
        @FileContents(name="/a.txt", resource="testFile.txt") File f
    ) throws IOException {
        assertEquals("a.txt", f.getName());
        assertEquals( List.of("abc"), Files.readAllLines(f.toPath()));
    }

    @Test
    public void loadAnAbsoluteResource(
        @FileContents(resource="/multiLineTestFile.txt") File f
    ) throws IOException {
        assertEquals("multiLineTestFile.txt", f.getName());
        assertEquals( List.of("abc", "123"), Files.readAllLines(f.toPath()));
    }


// GENERATE FOLDER

    @Test
    public void givenMethodParameter_injectBlankFile( @FileContents() File f ) throws FileNotFoundException {
        assertNotNull( f );

        assertTrue( f.exists() );
        assertFalse( f.isDirectory() );

        filesAwaitingDeletion.add( f );

        String fileContents = loadFile(f);

        assertEquals( "", fileContents );
    }

    @Test
    public void givenMethodParameter_injectFileWithContents( @FileContents(lines={"abc","def"}) File f ) throws FileNotFoundException {
        assertFileContentsEquals("abc\ndef", f);
    }

    @Test
    public void givenInstanceField_injectFileWithContents() throws FileNotFoundException {
        assertFileContentsEquals( "abc\ndef", fileWithContents );

        filesAwaitingDeletion.clear();
    }

    @SuppressWarnings("SameParameterValue")
    private void assertFileContentsEquals( String expected, File f ) throws FileNotFoundException {
        assertNotNull( f );

        assertTrue( f.exists() );
        assertFalse( f.isDirectory() );

        filesAwaitingDeletion.add( f );

        String fileContents = loadFile(f);

        assertEquals( expected, fileContents );
    }

    private String loadFile( File f ) throws FileNotFoundException {
        BufferedReader in = new BufferedReader(new FileReader(f));

        return in.lines().collect( Collectors.joining("\n") );
    }

    private void assertTwoDirectories( File f1, File f2) {
        assertNotNull( f1 );
        assertNotNull( f2 );

        assertTrue( f1.isDirectory() );
        assertTrue( f2.isDirectory() );

        assertTrue( f1.exists() );
        assertTrue( f2.exists() );

        assertNotEquals( f1, f2 );

        filesAwaitingDeletion.add( f1 );
        filesAwaitingDeletion.add( f2 );
    }

}
