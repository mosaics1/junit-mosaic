package mosaics.junit.lang;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringUtilsTest {
    @Nested
    public class SplitLineTestCases {
        @Test
        public void splitLine() {
            assertArrayEquals(new String[] {"abc def"}, StringUtils.splitLine("abc def"));
            assertArrayEquals(new String[] {"  abc def  "}, StringUtils.splitLine("  abc def  "));
            assertArrayEquals(new String[] {"  abc ", "def  "}, StringUtils.splitLine("  abc \ndef  "));
            assertArrayEquals(new String[] {"abc ", "def  "}, StringUtils.splitLine("  |abc \n|def  "));
        }
    }

}