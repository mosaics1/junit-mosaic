package mosaics.junit.lang.reflection;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JavaClassTest {
    @Nested
    public class UnparameterisedClassTests {

    }

    @Nested
    public class ParameterisedClassTests {
        @Nested
        public class ParameterisedFieldTests {
            @Test
            public void givenFieldWhoseTypeIsParameterised() {
                JavaClass       javaClass   = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaField       javaField   = javaClass.getField("field").get();


                assertEquals( "field", javaField.getName() );
                assertEquals( "mosaics.junit.lang.reflection.JavaClassTest$ClassParameterUsedInConstructor<java.lang.Integer>", javaField.getType().toString() );
            }

            @Test
            public void givenFieldWhoseTypeIsAGenericParameter() {
                JavaClass       topJavaClass   = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaField       topJavaField   = topJavaClass.getField("field").get();
                JavaField       childJavaField = topJavaField.getType().getField("v").get();


                assertEquals( "v", childJavaField.getName() );
                assertEquals( "java.lang.Integer", childJavaField.getType().toString() );
            }

            @Test
            public void givenFieldWithNestedGenerics() {
                JavaClass javaClass = JavaClass.of(NestedField.class);
                JavaField javaField = javaClass.getField("field").get();


                assertEquals( "field", javaField.getName() );
                assertEquals( "java.util.Optional<java.util.List<java.lang.String>>", javaField.getType().toString() );
                assertEquals( "[java.util.List<java.lang.String>]", javaField.getType().getClassGenerics().toString() );
            }
        }

        @Nested
        public class ParameterisedConstructorTests {
            @Test
            public void givenConstructorWhoseConstructorHasAParameterWithASpecifiedTypeParameter() {
                JavaClass       javaClass   = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaConstructor constructor = javaClass.getConstructors().get(0);


                List<JavaParameter> expectedParameters = List.of(
                    new JavaParameter(Optional.of(javaClass), JavaClass.of(ClassParameterUsedInConstructor.class, Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, constructor.getParameters() );
            }

            @Test
            public void givenConstructorWhoseConstructorHasAParameterIsASpecifiedTypeParameter() {
                JavaClass       topJavaClass     = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaField       topJavaField     = topJavaClass.getField("field").get();
                JavaConstructor childConstructor = topJavaField.getType().getConstructors().get(0);


                List<JavaParameter> expectedParameters = List.of(
                    new JavaParameter(Optional.of(JavaClass.of(ClassParameterUsedInConstructor.class)), JavaClass.of(Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, childConstructor.getParameters() );
            }
        }

        @Nested
        public class ParameterisedMethodTests {
            @Test
            public void givenMethodWhoseMethodHasAParameterWithASpecifiedTypeParameter() {
                JavaClass  javaClass = JavaClass.of(ClassParameterUsedInMethodDeclaration.class);
                JavaMethod method    = javaClass.getMethod("setValue", ClassParameterUsedInMethod.class).get();


                List<JavaParameter> expectedParameters = List.of(
                    new JavaParameter(Optional.of(javaClass), JavaClass.of(ClassParameterUsedInMethod.class, Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, method.getParameters() );
            }

            @Test
            public void givenMethodWhoseMethodHasAParameterIsASpecifiedTypeParameter() {
                JavaClass  topJavaClass = JavaClass.of(ClassParameterUsedInMethodDeclaration.class);
                JavaField  topJavaField = topJavaClass.getField("field").get();
                JavaMethod childMethod  = topJavaField.getType().getMethod("setValue", Integer.class).get();


                List<JavaParameter> expectedParameters = List.of(
                    new JavaParameter(Optional.of(JavaClass.of(ClassParameterUsedInMethod.class)), JavaClass.of(Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, childMethod.getParameters() );
            }
        }
    }


    public static class ClassParameterUsedInConstructorDeclaration {
        private ClassParameterUsedInConstructor<Integer> field;

        public ClassParameterUsedInConstructorDeclaration(ClassParameterUsedInConstructor<Integer> v) {
            this.field = v;
        }
    }

    public static class ClassParameterUsedInMethodDeclaration {
        private ClassParameterUsedInMethod<Integer> field;

        public ClassParameterUsedInMethodDeclaration(ClassParameterUsedInMethod<Integer> v) {
            this.field = v;
        }

        public ClassParameterUsedInMethod<Integer> getValue() {
            return field;
        }

        public void setValue(ClassParameterUsedInMethod<Integer> v) {
            this.field = v;
        }
    }

    public static class ClassParameterUsedInConstructor<T> {
        private T v;

        public ClassParameterUsedInConstructor( T v ) {
            this.v = v;
        }
    }

    public static class ClassParameterUsedInMethod<T> {
        private T v;

        public T getValue() {
            return v;
        }

        public void setValue(T v) {
            this.v = v;
        }
    }

    public static class NestedField {
        private Optional<List<String>> field;
    }
}
