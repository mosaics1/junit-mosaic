package mosaics.junit.lang;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ReflectionUtilsTest {
    @Test
    public void testIterate() {
        assertEquals( Collections.emptyList(), ReflectionUtils.iterate(null, v -> Optional.empty()).collect( Collectors.toList()));
        assertEquals(Collections.singletonList(1), ReflectionUtils.iterate(1, v -> Optional.empty()).collect(Collectors.toList()));
        assertEquals( Arrays.asList(1,2), ReflectionUtils.iterate(1, v -> v == 2 ? Optional.empty() : Optional.of(v+1)).collect(Collectors.toList()));
        assertEquals(Arrays.asList(1,2,3), ReflectionUtils.iterate(1, v -> v == 3 ? Optional.empty() : Optional.of(v+1)).collect(Collectors.toList()));
    }

    @Test
    public void testIsCastableTo() {
        assertTrue(ReflectionUtils.isCastableTo(String.class, String.class));
        assertTrue(ReflectionUtils.isCastableTo(String.class, Object.class));
        assertTrue(ReflectionUtils.isCastableTo(Object.class, Object.class));

        assertFalse(ReflectionUtils.isCastableTo(Object.class, String.class));
    }

    @Test
    public void testToClass() {
        assertSame( String.class, ReflectionUtils.toClass(String.class) );
        assertSame( Integer.class, ReflectionUtils.toClass(Integer.class) );
        assertSame( Integer[].class, ReflectionUtils.toClass(Integer[].class) );
    }

    @Test
    public void testGetCallersStackFrame() {
        // the test is one function call deep because reflection is used to find this method as the caller
        doTestGetCallersStackFrame();
    }

    private void doTestGetCallersStackFrame() {
        StackWalker.StackFrame frame = ReflectionUtils.getCallersStackFrame();

        assertEquals( ReflectionUtilsTest.class.getName(), frame.getClassName() );
        assertEquals( "testGetCallersStackFrame", frame.getMethodName() );
    }
}
