package mosaics.junit.di;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;


@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
public class Book {
    private String  title;
    private Throwable ex;
    private boolean hasBeenReleased;


    public Book( String title ) {
        this( title, null, false );
    }


    public void free() {
        this.hasBeenReleased = true;
    }

    public void handleException( Throwable ex ) {
        this.ex = ex;
    }
}
