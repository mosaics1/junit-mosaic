//package mosaics.junit.plugins.store;
//
//import mosaics.junit.plugins.ObjectKey;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//
//@SuppressWarnings("unchecked")
//public class ObjectKeyTest {
//
//
//// CLASS CONSTRUCTOR
//
//    @Test
//    public void givenAnInterface_usingClassConstructorCreateAnInstance_expectInterfaceToBeUsedWhenMatchingWhereDIOccurs() {
//        ObjectKey<A> a = new ObjectKey<>("a", A.class );
//
//        assertTrue( a.hasParent(A.class) );
//        assertFalse( a.hasParent(B.class) );
//        assertFalse( a.hasParent(OA.class) );
//        assertFalse( a.hasParent(OB.class) );
//    }
//
//    @Test
//    public void givenALeafInterface_usingClassConstructorCreateAnInstance_expectInterfaceToBeUsedWhenMatchingWhereDIOccurs() {
//        ObjectKey<B> b = new ObjectKey<>("ob", B.class );
//
//        assertTrue( b.hasParent(A.class) );
//        assertTrue( b.hasParent(B.class) );
//        assertFalse( b.hasParent(OA.class) );
//        assertFalse( b.hasParent(OB.class) );
//    }
//
//    @Test
//    public void givenAnInstanceOfAnInterface_usingClassConstructorCreateAnInstance_expectObjectToMatchItsInterfaces() {
//        ObjectKey<OA> oa = new ObjectKey<>("oa", OA.class );
//
//        assertTrue( oa.hasParent(A.class) );
//        assertFalse( oa.hasParent(B.class) );
//        assertTrue( oa.hasParent(OA.class) );
//        assertFalse( oa.hasParent(OB.class) );
//    }
//
//    @Test
//    public void givenAnInstanceOfALeafInterface_usingClassConstructorCreateAnInstance_expectObjectToMatchItsInterfaces() {
//        ObjectKey<OB> ob = new ObjectKey<>("ob", OB.class );
//
//        assertTrue( ob.hasParent(A.class) );
//        assertTrue( ob.hasParent(B.class) );
//        assertFalse( ob.hasParent(OA.class) );
//        assertTrue( ob.hasParent(OB.class) );
//    }
//
/////
//
//    @Test
//    public void testCanHoldValueOfType() {
//        assertTrue( new ObjectKey<>("k",B.class).canHoldValueOfType(B.class) );
//        assertTrue( new ObjectKey<>("k",B.class).canHoldValueOfType(OB.class) );
//        assertTrue( new ObjectKey<>("k",A.class).canHoldValueOfType(OB.class) );
//        assertTrue( new ObjectKey<>("k",A.class).canHoldValueOfType(OA.class) );
//
//        assertFalse( new ObjectKey<>("k",B.class).canHoldValueOfType(OA.class) );
//        assertFalse( new ObjectKey<>("k",B.class).canHoldValueOfType(A.class) );
//        assertTrue( new ObjectKey<>("k",B.class).canHoldValueOfType(B.class) );
//        assertTrue( new ObjectKey<>("k",B.class).canHoldValueOfType(OB.class) );
//    }
//
//    public static interface A {
//
//    }
//
//    public static interface B extends A {
//
//    }
//
//    public static class OA implements A {
//
//    }
//
//    public static class OB implements B {
//
//    }
//}
