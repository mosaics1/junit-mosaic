//package mosaics.junit.plugins.factory;
//
//import mosaics.junit.plugins.Book;
//import mosaics.junit.plugins.ObjectKey;
//import mosaics.junit.plugins.store.CachedObjectFetcher;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertNull;
//import static org.junit.jupiter.api.Assertions.assertSame;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.junit.jupiter.api.Assertions.fail;
//
//
//@SuppressWarnings("ConstantConditions")
//public class ObjectFactoryTest {
//
//    private ObjectFactory factory = new ObjectFactory();
//    private CachedObjectFetcher fetcher = Mockito.mock(CachedObjectFetcher.class);
//
//
//    @Test
//    public void givenMatchingRegistration_expectNoAllocation() {
//        assertEquals( Optional.empty(), factory.create(new ObjectKey<>("name",String.class),fetcher) );
//    }
//
//    @Test
//    public void givenMatchedRegistration_expectAllocation() {
//        factory.<Book>registerHandler(
//            key -> key.hasParent(Book.class),
//            (key,cache) -> {
//                Book book = new Book(key.getName());
//
//                return new AllocatedObject<>( key.getName(), book )
//                    .withDestructor( book::free )
//                    .withExceptionHandler( book::handleException );
//            }
//        );
//
//        Optional<AllocatedObject<Book>> actual = factory.create(new ObjectKey<>("name",Book.class),fetcher);
//        Book expected = new Book("name");
//
//        assertEquals( expected, actual.get().getInstance() );
//    }
//
//    @Test
//    public void givenMatchedRegistration_expectValidExceptionHandler() {
//        factory.<Book>registerHandler(
//            key -> key.hasParent(Book.class),
//            (key,cache) -> {
//                Book book = new Book(key.getName());
//
//                return new AllocatedObject<>( key.getName(), book )
//                    .withDestructor( book::free )
//                    .withExceptionHandler( book::handleException );
//            }
//        );
//
//        AllocatedObject<Book> ref  = factory.create(new ObjectKey<>("name",Book.class),fetcher).get();
//        Book                  book = ref.getInstance();
//
//        assertNull( book.getEx() );
//
//        RuntimeException ex = new RuntimeException( "foo" );
//        ref.getExceptionHandler().accept( ex );
//
//        assertSame( ex, book.getEx() );
//    }
//
//    @Test
//    public void givenMatchedRegistration_expectValidDestructor() {
//        factory.<Book>registerHandler(
//            key -> key.hasParent(Book.class),
//            (key,cache) -> {
//                Book book = new Book(key.getName());
//
//                return new AllocatedObject<>( key.getName(), book )
//                    .withDestructor( book::free )
//                    .withExceptionHandler( book::handleException );
//            }
//        );
//
//        AllocatedObject<Book> ref  = factory.create(new ObjectKey<>("name",Book.class),fetcher).get();
//        Book                  book = ref.getInstance();
//
//        assertFalse( book.isHasBeenReleased() );
//
//        ref.getDestructor().run();
//
//        assertTrue( book.isHasBeenReleased() );
//    }
//
//    @Test
//    public void givenMatchedRegistration_tryToInvokeDestructorTwice_expectError() {
//        factory.<Book>registerHandler(
//            key -> key.hasParent(Book.class),
//            (key,cache) -> {
//                Book book = new Book(key.getName());
//
//                return new AllocatedObject<>( key.getName(), book )
//                    .withDestructor( book::free )
//                    .withExceptionHandler( book::handleException );
//            }
//        );
//
//        AllocatedObject<Book> ref = factory.create(new ObjectKey<>("name",Book.class),fetcher).get();
//
//        ref.getDestructor().run();
//
//
//        try {
//            ref.getDestructor().run();
//
//            fail( "expected IllegalStateException" );
//        } catch ( IllegalStateException e ) {
//            assertEquals( "'name' has already been released", e.getMessage() );
//        }
//    }
//
//}
