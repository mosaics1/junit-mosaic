package mosaics.junit;

import mosaics.junit.di.ObjectFactory;
import mosaics.junit.lang.reflection.JavaClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


// TODO
// 1) how to test releasing values
// 2) how to ensure tests that throw exceptions still release values

@SuppressWarnings("unused")
@ExtendWith( JMExtensionTest.InjectStringAndIntValuesExtension.class )
public class JMExtensionTest {
    private static String staticField1;

    private String instanceField1;
    private String instanceField2;

    private Integer integerInstanceField1;

    private boolean booleanInstanceField1;
    private byte byteInstanceField1;
    private char charInstanceField1;
    private short shortInstanceField1;
    private int intInstanceField1;
    private long longInstanceField1;
    private float floatInstanceField1;
    private double doubleInstanceField1;

    private static final char POUND_SYMBOL = '\u00A3';

    @BeforeAll
    public static void beforeAll() {
        assertEquals( "staticField1", staticField1 );
    }

    @BeforeEach
    public void beforeEach() {
        assertEquals( "staticField1", staticField1 );

        assertEquals( "instanceField1", instanceField1 );
        assertEquals( "instanceField2", instanceField2 );
        assertEquals( 42, integerInstanceField1 );

        assertEquals( true, booleanInstanceField1 );
        assertEquals( 3, byteInstanceField1 );
        assertEquals( (short) 3, shortInstanceField1 );
        assertEquals( POUND_SYMBOL, charInstanceField1 );
        assertEquals( 42, intInstanceField1 );
        assertEquals( 42L, longInstanceField1 );
        assertEquals( 1.1f, floatInstanceField1 );
        assertEquals( 1.1d, doubleInstanceField1 );
    }

    @AfterAll
    public static void afterAll() {
        assertEquals( "staticField1", staticField1 ); // still set
    }

    @AfterEach
    public void afterEach() {
        // expect values to still be set
        assertEquals( "staticField1", staticField1 );

        assertEquals( "instanceField1", instanceField1 );
        assertEquals( "instanceField2", instanceField2 );
        assertEquals( 42, integerInstanceField1 );

        assertEquals( true, booleanInstanceField1 );
        assertEquals( 3, byteInstanceField1 );
        assertEquals( (short) 3, shortInstanceField1 );
        assertEquals( POUND_SYMBOL, charInstanceField1 );
        assertEquals( 42, intInstanceField1 );
        assertEquals( 42L, longInstanceField1 );
        assertEquals( 1.1f, floatInstanceField1 );
        assertEquals( 1.1d, doubleInstanceField1 );
    }

    @Test
    public void testInjectingStaticFields() {
        assertEquals( "staticField1", staticField1 );
    }

    @Test
    public void testInjectingInstanceFields() {
        assertEquals( "instanceField1", instanceField1 );
        assertEquals( "instanceField2", instanceField2 );
        assertEquals( 42, integerInstanceField1 );

        assertEquals( true, booleanInstanceField1 );
        assertEquals( 3, byteInstanceField1 );
        assertEquals( (short) 3, shortInstanceField1 );
        assertEquals( POUND_SYMBOL, charInstanceField1 );
        assertEquals( 42, intInstanceField1 );
        assertEquals( 42L, longInstanceField1 );
        assertEquals( 1.1f, floatInstanceField1 );
        assertEquals( 1.1d, doubleInstanceField1 );
    }

    @Test
    public void showInstanceFieldsGetInjected2(String param1, String param2, int param3) {
        assertEquals( "param1", param1 );
        assertEquals( "param2", param2 );
        assertEquals( 42, param3 );
    }

    @Nested
    public class NestedClassTestCases {
        private String nestedInstanceField1;
        private String nestedInstanceField2;


        @Test
        public void nestedInstanceFields() {
            assertEquals( "nestedInstanceField1", nestedInstanceField1 );
            assertEquals( "nestedInstanceField2", nestedInstanceField2 );
        }

        @Test
        public void methodParametersOnNestedClass(String param1) {
            assertEquals( "param1", param1 );
        }

        @Nested
        public class NestedNestedTestCases {
            private String nestedNestedField1;

            @Test
            public void nestedNestedInstanceFields() {
                assertEquals( "nestedNestedField1", nestedNestedField1 );
            }
        }
    }

    @Nested
    public class ParameterisedTestCases {
        private List<String> stringList;

        @Test
        public void parameterisedInstanceField() {
            assertEquals(List.of("str", "str"), stringList);
        }
    }

    public static class InjectStringAndIntValuesExtension extends JMExtension {
        private static final Set<Class> supportedTypes = Set.of(
            String.class, boolean.class, byte.class, short.class, int.class, Integer.class,
            long.class, float.class, double.class, char.class, List.class
        );

        public InjectStringAndIntValuesExtension() {
            super(
                () -> ObjectFactory.newFactory(
                    v -> supportedTypes.contains(v.type().getJdkClass()),
                    (factory,v) -> {
                        Class type = v.type().getJdkClass();

                        if ( type == String.class ) {
                            String name = v.name();
                            return name == null ? "str" : name;
                        } else if ( type == boolean.class ) {
                            return true;
                        } else if ( type == char.class ) {
                            return POUND_SYMBOL;
                        } else if ( type == byte.class ) {
                            return (byte) 3;
                        } else if ( type == short.class ) {
                            return (short) 3;
                        } else if ( type == float.class ) {
                            return 1.1f;
                        } else if ( type == double.class ) {
                            return 1.1d;
                        } else if ( type == List.class ) {
                            JavaClass pt = v.type().getClassGenerics().get(0);

                            return List.of(factory.create(pt),factory.create(pt));
                        } else {
                            return 42;
                        }
                    },
                    o -> {}
                )
            );
        }
    }
}
