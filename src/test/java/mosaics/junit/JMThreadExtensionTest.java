package mosaics.junit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.assertThrows;


public class JMThreadExtensionTest {

    @Test
    public void givenATestThatFiresUpAndShutsDownATestExpectTheTestToPass() {
        JUnitHarness.runUnitTests( StartAndStopThreadTestCases.class );
    }

    @Test
    public void givenATestThatFiresUpAndDoesNotShutTheThreadDownExpectError() {
        assertThrows( JUnitHarness.MultipleFailuresError.class, () -> JUnitHarness.runUnitTests( StartAndDoNotStopThreadTestCases.class));
    }

    @Test
    public void givenBeforeEachThatStartsUpAndShutsDownAThreadExpectTheTestToPass() {
        JUnitHarness.runUnitTests( BeforeEachStartAndStopThreadTestCases.class );
    }

    @Test
    public void givenBeforeEachThatStartsUpAndDoesNotShutDownAThreadExpectTheTestToFail() {
        assertThrows( JUnitHarness.MultipleFailuresError.class, () -> JUnitHarness.runUnitTests(BeforeEachStartAndDoNotStopThreadTestCases.class));
    }

    @Test
    public void givenBeforeAllThatStartsUpAndShutsDownAThreadExpectTheTestToPass() {
        JUnitHarness.runUnitTests( BeforeAllStartAndStopThreadTestCases.class );
    }

    @Test
    public void givenBeforeAllThatStartsUpAndDoesNotShutDownAThreadExpectTheTestToFail() {
        assertThrows( JUnitHarness.MultipleFailuresError.class, () -> JUnitHarness.runUnitTests( BeforeAllStartAndDoNotStopThreadTestCases.class));
    }


    @Tag("internal")
    @ExtendWith( JMThreadExtension.class )
    static class StartAndStopThreadTestCases {
        @Test
        public void givenATestThatFiresUpAndShutsDownImmediatelyATestExpectTheTestToPass() {
            new Thread("StartAndStopThreadTestCases.givenATestThatFiresUpAndShutsDownImmediatelyATestExpectTheTestToPass()") {
                @Override
                public void run() {}
            }.start();
        }

        @Test
        public void givenATestThatFiresUpAndShutsDownAfterShortPauseATestExpectTheTestToPass() {
            new Thread("StartAndStopThreadTestCases.givenATestThatFiresUpAndShutsDownAfterShortPauseATestExpectTheTestToPass()") {
                @Override
                public void run() {
                    try {
                        Thread.sleep( 200 );
                    } catch ( InterruptedException e ) {
                        throw new RuntimeException(e);
                    }
                }
            }.start();
        }
    }

    @Tag("internal")
    @ExtendWith( JMThreadExtension.class )
    static class StartAndDoNotStopThreadTestCases {
        @Test
        public void startThreadAndLeaveItRunningInATestMethod() {
            new Thread("StartAndDoNotStopThreadTestCases.startThreadAndLeaveItRunningInATestMethod()") {
                @Override
                public void run() {
                    while (true);
                }
            }.start();
        }
    }

    @Tag("internal")
    @ExtendWith( JMThreadExtension.class )
    static class BeforeEachStartAndStopThreadTestCases {
        private volatile boolean shutdownRequestReceived;

        @BeforeEach
        public void beforeEach() {
            new Thread("BeforeEachStartAndDoNotStopThreadTestCases.beforeEach()") {
                @Override
                public void run() {
                    while (!shutdownRequestReceived);
                }
            }.start();
        }

        @AfterEach
        public void afterEach() {
            shutdownRequestReceived = true;
        }

        @Test
        public void givenATestThatFiresUpAndShutsDownImmediatelyATestExpectTheTestToPass() {
        }
    }

    @Tag("internal")
    @ExtendWith( JMThreadExtension.class )
    static class BeforeEachStartAndDoNotStopThreadTestCases {
        @BeforeEach
        public void beforeEach() {
            new Thread("BeforeEachStartAndDoNotStopThreadTestCases.beforeEach()") {
                @Override
                public void run() {
                    while (true);
                }
            }.start();
        }

        @Test
        public void startThreadAndLeaveItRunningInATestMethod() {

        }
    }

    @Tag("internal")
    @ExtendWith( JMThreadExtension.class )
    static class BeforeAllStartAndStopThreadTestCases {
        private volatile static boolean shutdownRequestReceived;

        @BeforeAll
        public static void beforeAll() {
            new Thread("BeforeAllStartAndStopThreadTestCases.beforeEach()") {
                @Override
                public void run() {
                    while (!shutdownRequestReceived);
                }
            }.start();
        }

        @AfterAll
        public static void afterAll() {
            shutdownRequestReceived = true;
        }

        @Test
        public void givenATestThatFiresUpAndShutsDownImmediatelyATestExpectTheTestToPass() {
        }
    }


    @Tag("internal")
    @ExtendWith( JMThreadExtension.class )
    static class BeforeAllStartAndDoNotStopThreadTestCases {
        @BeforeAll
        public static void beforeAll() {
            new Thread("BeforeAllStartAndDoNotStopThreadTestCases.beforeEach()") {
                @Override
                public void run() {
                    while (true);
                }
            }.start();
        }

        @Test
        public void startThreadAndLeaveItRunningInATestMethod() {

        }
    }
}
