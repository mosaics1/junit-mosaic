package mosaics.junit;

import lombok.Value;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import static mosaics.junit.JMAssertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;


public class JMAssertionsTest {

    @Nested
    public class SpinUntilReleasedTestCases {
        @Test
        public void givenDTOInWeakRefWithNoStrongRefs_expectSpinUntilDTOIsGCed() {
            DTO                dto = new DTO("foo");
            WeakReference<DTO> ref = new WeakReference<>( dto );

            dto = null;

            JMAssertions.spinUntilReleased( ref );

            assertNull( ref.get() );
        }

        @Test
        public void givenDTOWithAStrongRef_expectSpinToTimeOut() {
            DTO                dto = new DTO("foo");
            WeakReference<DTO> ref = new WeakReference<>( dto );

            try {
                JMAssertions.spinUntilReleased( ref, Duration.ofMillis( 300 ) );
            } catch ( AssertionFailedError ex ) {
                assertEquals( "Object not released within 0.3s", ex.getMessage() );
            }

            assertSame( dto, ref.get() );
        }
    }

    @Nested
    public class SpinUntilTrueTestCases {
        @Test
        public void decrementCounter_expectSpinUntilTrueToPass() {
            AtomicInteger counter = new AtomicInteger( 20 );

            JMAssertions.spinUntilTrue( () -> counter.decrementAndGet() <= 0 );
            assertEquals( 0, counter.get() );
        }
    }

    @Nested
    public class SpinUntilFalseTestCases {
        @Test
        public void decrementCounter_expectSpinUntilTrueToPass() {
            AtomicInteger counter = new AtomicInteger( 20 );

            JMAssertions.spinUntilFalse( () -> counter.decrementAndGet() > 0 );
            assertEquals( 0, counter.get() );
        }
    }

    @Nested
    public class SpinUntilStableTestCases {
        @Test
        public void decrementCounter_expectSpinUntilTrueToPass() {
            AtomicInteger counter = new AtomicInteger( 20 );

            JMAssertions.spinUntilStable( () -> counter.get() == 10 ? 10 : counter.decrementAndGet() );
            assertEquals( 10, counter.get() );
        }
    }

    @Nested
    public class AwaitLatchTestCases {
        @Test
        public void decrementCounter_expectSpinUntilTrueToPass() {
            CountDownLatch latch = new CountDownLatch( 1 );

            new Thread( latch::countDown ).start();

            JMAssertions.awaitLatch( latch );
            assertEquals( 0, latch.getCount() );
        }
    }

    @Nested
    public class AssertThrowsTestCases {
        @Test
        public void successCases() {
            assertThrows(IllegalArgumentException.class, "msg", () -> {throw new IllegalArgumentException("msg");});
            assertThrows(RuntimeException.class, "msg 2", () -> {throw new RuntimeException("msg 2");});
            assertThrows(Exception.class, "msg 3", () -> {throw new RuntimeException("msg 3");});
        }

        @Test
        public void exceptionTypeDoesNotMatch_expectError() {
            try {
                assertThrows( IOException.class, "msg", () -> {
                    throw new IllegalArgumentException( "msg" );
                } );
                fail( "expected AssertionFailedError" );
            } catch ( AssertionFailedError ex ) {
                assertEquals("Caught java.lang.IllegalArgumentException when expecting java.io.IOException ==> expected: <true> but was: <false>", ex.getMessage());
            }
        }

        @Test
        public void exceptionMsgDoesNotMatch_expectError() {
            try {
                assertThrows( IllegalArgumentException.class, "msg 2", () -> {
                    throw new IllegalArgumentException( "msg" );
                } );
                fail( "expected AssertionFailedError" );
            } catch ( AssertionFailedError ex ) {
                assertEquals("expected: <msg 2> but was: <msg>", ex.getMessage());
            }
        }
    }

    @Value
    public static class DTO {
        private String name;
    }
}
