package mosaics.junit.jvm;

import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ThreadWatcherTest {

    @Test
    public void givenNoInitialThreadsAndNoExitThreads_expectNoError() {
        ThreadWatcher threadWatcher = new ThreadWatcher( Collections::emptySet );

        threadWatcher.startThreadCleanupCriticalRegion();
        threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();
    }

    @Test
    public void givenSomeInitialThreadsAndNoExitThreads_expectNoError() {
        Supplier<Set<ThreadIdentifier>> threadCaptureMockFunction = mockThreadCapture(
            Set.of(fakeThreadId(1, "thread1", FakeThread1.class)),
            Set.of()
        );

        ThreadWatcher threadWatcher = new ThreadWatcher( threadCaptureMockFunction );

        threadWatcher.startThreadCleanupCriticalRegion();
        threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();
    }

    @Test
    public void givenSomeInitialThreadsAndTheSameExitThreads_expectNoError() {
        Supplier<Set<ThreadIdentifier>> threadCaptureMockFunction = mockThreadCapture(
            Set.of(fakeThreadId(1, "thread1", FakeThread1.class)),
            Set.of(fakeThreadId(1, "thread1", FakeThread1.class))
        );

        ThreadWatcher threadWatcher = new ThreadWatcher( threadCaptureMockFunction );

        threadWatcher.startThreadCleanupCriticalRegion();
        threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();
    }

    @Test
    public void givenSomeInitialThreadsAndOneNewExitThreads_expectError() {
        Supplier<Set<ThreadIdentifier>> threadCaptureMockFunction = mockThreadCapture(
            Set.of(fakeThreadId(1, "thread1", FakeThread1.class)),
            Set.of(fakeThreadId(2, "thread2", FakeThread2.class))
        );

        ThreadWatcher threadWatcher = new ThreadWatcher( threadCaptureMockFunction );

        threadWatcher.startThreadCleanupCriticalRegion();

        try {
            threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();

            throw new RuntimeException("expected AssertionFailedError");
        } catch ( AssertionFailedError ex ) {
            assertEquals(
                "1 thread is still running after the test completed: " + System.lineSeparator() +
                "    Thread(threadName=thread2, class=mosaics.junit.jvm.ThreadWatcherTest$FakeThread2)" + System.lineSeparator(),
                ex.getMessage()
            );
        }
    }

    @Test
    public void givenSomeInitialThreadsAndOneNewExitThreadsThatIsWithinTheExcludeList_expectNoError() {
        Supplier<Set<ThreadIdentifier>> threadCaptureMockFunction = mockThreadCapture(
            Set.of(
                fakeThreadId(1, "thread1", FakeThread1.class),
                fakeThreadId(2, "thread2", FakeThread2.class),
                fakeThreadId(3, "thread3", FakeThread3.class)
            ),
            Set.of(fakeThreadId(2, "thread2", FakeThread2.class))
        );

        ThreadWatcher threadWatcher = new ThreadWatcher( threadCaptureMockFunction );

        threadWatcher.startThreadCleanupCriticalRegion();
        threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();
    }

    @Test
    public void givenAnExitThreadThatShutsDownWithinTheGracePeriod_expectNoError() {
        Supplier<Set<ThreadIdentifier>> threadCaptureMockFunction = mockThreadCapture(
            Set.of(
                fakeThreadId(1, "thread1", FakeThread1.class),
                fakeThreadId(3, "thread3", FakeThread3.class)
            ),
            Set.of(
                fakeThreadId(2, "thread2", FakeThread2.class, 3)
            )
        );

        ThreadWatcher threadWatcher = new ThreadWatcher( threadCaptureMockFunction );

        threadWatcher.startThreadCleanupCriticalRegion();
        threadWatcher.exitThreadCleanupCriticalRegionAndEnsureThatNoNewThreadsHaveBeenLeftRunning();
    }


    private Supplier<Set<ThreadIdentifier>> mockThreadCapture( Set<ThreadIdentifier>...perCallResults ) {
        Iterator<Set<ThreadIdentifier>>        it                = List.of(perCallResults).iterator();
        AtomicReference<Set<ThreadIdentifier>> currentIdentifier = new AtomicReference<>();

        return () -> {
            if ( it.hasNext() ) {
                currentIdentifier.set( it.next() );
            }

            return currentIdentifier.get();
        };
    }

    private static class FakeThread1 extends Thread {}
    private static class FakeThread2 extends Thread {}
    private static class FakeThread3 extends Thread {}

    private static ThreadIdentifier fakeThreadId(long id, String threadName, Class threadClass ) {
        return new ThreadIdentifier( id, threadName, threadClass ) {
            public boolean hasStopped() {
                return false;
            }
        };
    }

    private static ThreadIdentifier fakeThreadId(long id, String threadName, Class threadClass, int numRequestsUntilShutdown ) {
        AtomicInteger counter = new AtomicInteger(numRequestsUntilShutdown);

        return new ThreadIdentifier( id, threadName, threadClass ) {
            public boolean hasStopped() {
                return counter.decrementAndGet() <= 0;
            }
        };
    }
}
