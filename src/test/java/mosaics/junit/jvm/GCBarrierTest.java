package mosaics.junit.jvm;

import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class GCBarrierTest {

    @Test
    public void spinUntilOneObjectIsReleased() {
        GCBarrier gcBarrier = new GCBarrier();

        gcBarrier.push( new Object() );

        gcBarrier.spinUntilAllObjectsHaveBeenReleased();
    }

    @Test
    public void spinUntilTwoObjectsAreReleased() {
        GCBarrier gcBarrier = new GCBarrier();

        gcBarrier.push( new Object() );
        gcBarrier.push( new Object() );

        gcBarrier.spinUntilAllObjectsHaveBeenReleased();
    }

    @Test
    public void spinUntilThreeObjectsAreReleased() {
        GCBarrier gcBarrier = new GCBarrier();

        gcBarrier.push( new Object() );
        gcBarrier.push( new Object() );
        gcBarrier.push( new Object() );

        gcBarrier.spinUntilAllObjectsHaveBeenReleased();
    }

    @Test
    public void errorWhenOneObjectIsNotReleased() {
        GCBarrier gcBarrier = new GCBarrier();

        gcBarrier.push( new Object() );
        gcBarrier.push( new Object() );

        Object strongRef = new Object();
        gcBarrier.push( strongRef );

        try {
            gcBarrier.spinUntilAllObjectsHaveBeenReleased();
            fail("expected AssertionFailedError");
        } catch ( AssertionFailedError ex ) {
            assertEquals(
                """
                1 object(s) have not been released
                    an instance of class java.lang.Object was allocated at mosaics.junit.jvm.GCBarrierTest::errorWhenOneObjectIsNotReleased
                """.stripIndent(), ex.getMessage());
        }
    }

    @Test
    public void errorWhenTwoObjectsAreNotReleased() {
        GCBarrier gcBarrier = new GCBarrier();

        gcBarrier.push( new Object() );
        Object strongRef1 = new Object();
        gcBarrier.push( strongRef1 );

        Object strongRef2 = new Object();
        gcBarrier.push( strongRef2 );

        try {
            gcBarrier.spinUntilAllObjectsHaveBeenReleased();
            fail("expected AssertionFailedError");
        } catch ( AssertionFailedError ex ) {
            assertEquals(
                """
                2 object(s) have not been released
                    an instance of class java.lang.Object was allocated at mosaics.junit.jvm.GCBarrierTest::errorWhenTwoObjectsAreNotReleased
                    an instance of class java.lang.Object was allocated at mosaics.junit.jvm.GCBarrierTest::errorWhenTwoObjectsAreNotReleased
                """.stripIndent(), ex.getMessage());
        }
    }

}
