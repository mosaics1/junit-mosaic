
# JUnit Mosaic

JUnitMosaic is a set of JUnit 5 extensions that help to make automated testing faster and more enjoyable
to write.

In brief, the features offered are:

* [tools for asserting that objects get released](docs/usage/gc.md)
* [tools for asserting that threads get cleaned up](docs/usage/threads.md)
* [tools for creating and cleaning up files used during testing](docs/usage/files.md)
* [tools for providing random data to a test](docs/usage/random.md) 
* [tools for testing concurrent code](docs/usage/concurrency.md)
