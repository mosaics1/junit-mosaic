plugins {
    `java-library`
    `maven-publish`
    id("com.github.ben-manes.versions").version("0.42.0")
}

repositories {
    mavenCentral()
    mavenLocal()
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17

    withJavadocJar()
    withSourcesJar()
}

dependencies {
    val junitVersion = "5.8.2"
    val junitPlatformLauncherVersion = "1.8.2"
    val lombokVersion = "1.18.22"

    api("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    api("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    api("org.junit.jupiter:junit-jupiter-params:$junitVersion")
    implementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
    implementation("org.junit.platform:junit-platform-launcher:$junitPlatformLauncherVersion")
    api("org.mockito:mockito-core:4.3.1")
    api("nl.jqno.equalsverifier:equalsverifier:3.9")
    api( "com.thedeanda:lorem:2.1" )

    compileOnly("org.projectlombok:lombok:$lombokVersion")
    annotationProcessor("org.projectlombok:lombok:$lombokVersion")
    testCompileOnly("org.projectlombok:lombok:$lombokVersion")
    testAnnotationProcessor("org.projectlombok:lombok:$lombokVersion")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.mosaics"
            artifactId = "junit-mosaic"
            version = System.getenv("CI_PIPELINE_ID") ?: "SNAPSHOT"

            from(components["java"])

            repositories {
                maven {
                    name = "GitLab"
                    url = uri("https://gitlab.com/api/v4/projects/19218851/packages/maven")

                    credentials(HttpHeaderCredentials::class) {
                      name = "Job-Token"
                      value = System.getenv("CI_JOB_TOKEN")
                    }

                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }

            pom {
                name.set(rootProject.name)
                description.set(
                        "JUnitMosaic is a set of JUnit 5 extensions that help to make automated " +
                                "testing faster and more enjoyable to write."
                )
                url.set("https://gitlab.com/mosaics1/junit-mosaic")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("chrisk")
                        name.set("Chris Kirk")
                        email.set("kirkch@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitlab.com:mosaics1/junit-mosaic.git")
                    developerConnection.set("git@gitlab.com:mosaics1/junit-mosaic.git")
                    url.set("https://gitlab.com/mosaics1/junit-mosaic")
                }
            }
        }
    }
}

tasks.javadoc {
    if (JavaVersion.current().isJava9Compatible) {
        (options as StandardJavadocDocletOptions).addBooleanOption("html5", true)
    }
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform() {
        excludeTags("internal") // tests marked with 'internal' are not to be auto run - they have their own test harness that triggers them
    }
}

// (enable jvm preview features) see https://docs.gradle.org/current/userguide/building_java_projects.html#sec:feature_preview
tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}

tasks.withType<Test> {
    systemProperty( "file.encoding", "UTF-8" )

    jvmArgs("--enable-preview")

    testLogging.showExceptions = true
    testLogging.exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    testLogging.showCauses = true
    testLogging.showStackTraces = true
    testLogging.showStandardStreams = getBooleanSetting("debugFlag")

    // https://stackoverflow.com/questions/56628983/how-to-give-system-property-to-my-test-via-kotiln-gradle-and-d/62629332#62629332
    systemProperties(System.getProperties().toMap() as Map<String,Object>)
}

tasks.withType<JavaExec> {
}

tasks.withType<Javadoc> {
    options.encoding = "UTF-8"

    val javadocOptions = options as CoreJavadocOptions

    javadocOptions.addStringOption("source", "17")
    javadocOptions.addBooleanOption("Xdoclint:none", true)  // todo remove this and fix the errors, test with gradlew javadoc
}


// Place settings within settings.gradle.kts
fun getBooleanSetting(name: String, defaultValue: Boolean = false): Boolean {
    if ( !gradle.rootProject.extra.has(name) ) {
        return defaultValue
    }

    val strValue = gradle.rootProject.extra.get(name).toString()

    return strValue.toBoolean()
}
