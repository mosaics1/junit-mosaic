#!/bin/bash

set -eux

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"

$DIR/gradlew check -Dmosaics.junit.TimeoutSeconds=40
